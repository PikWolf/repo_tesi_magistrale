FLUTTUAZIONI DI NON EQUILIBRIO DURANTE DIFFUSIONE IN LIQUIDI COMPLESSI (NEUF-DIX SPACE PROJECT)

GENERALITÀ

I processi di diffusione e diffusione termica in una miscela liquida sono accompagnati da fluttazioni
di non equilibrio a lungo raggio, la cui ampiezza è ordini di grandezza maggiore di quella delle 
fluttuazioni di equilibrio. L'ampiezza quadratica media fluttuazioni di non equilibrio (NEF) presenta
un comportamento scale-free a legge di potenza q^-4 come funzione del vettor d'onda q, ma la presenza 
della gravità impedisce la divergenza a piccoli q. In microgravità le NEF si sviluppano completamente
e spannano (occupano, coprono) le scale di lunghezza disponibili fino alle dimensioni macroscopiche 
del sistema in direzione parallela al gradiente applicato (gradiente che genera la condizione di non
equilibrio).

Si è visto che:
  - le correnti di massa microscopiche determinate dalle NEF danno luogo a un flusso macroscopico di Fick.
    Di conseguenza le NEF rappresentano il meccanismo intrinseco che dà luogo alla diffusione alla scala
    mesoscopica.
  - teoria ed esperimenti fatti finora si concentrano su condizioni molto semplificate (ideali): stati 
    stazionari, piccoli gradienti, sistemi diluiti. Tuttavia sembra essere importante generalizzare.

Scopo del progetto: studiare le NEF in assenza di gravità in liquidi complessi, in condizioni di:
  - transient diffusion
  - concentrated samples
  - multi-component mixtures
  - large gradients
Il focus è sullo studio delle NEF in soluzioni di polimeri complesse e sospensioni colloidali.

Per poter osservare il comportamento scale-free di queste fluttuazioni, l'assenza di gravità è
fondamentale, in quanto la gravità soffoca le NEF a lunghezze d'onda elevate.
Dettaglio dei problemi recenti di cui si occuperà il progetto:
  - la predizione teorica delle forze di Casimir indotte da NEF
  - la comprensione delle NEF in miscele multicomponenti, in relazione ai coefficienti di trasporto.
  - la comprensione delle NEF in soluzioni polimeriche, in relazione al loro comportamento nei pressi
    di una Glass transition
  - la comprensione delle NEF in sospensioni colloidali concentrate (problema collegato alla 
    rivelazione delle forze di Casimir)

Apparato sperimentale: shadowgraph, che è n grado di acquisire immagini di fluttuazioni dell'indice di
rifrazione in un campione di liquido soggetto a un thermal stress. Imporre un gradiente esterno di
temperatura è il modo più facile e semplice di mantenere uno stato steady di non equilibrio in un 
liquido.

NEF

In presenza di un gradiente macroscopico (di temperatura o concentrazione), le fluttuazioni di non
equilibrio che si formano sono molto diverse dalle fluttuazioni di equilibrio dovute ad agitazione
termica, le quali sono fluttuazioni locali nelle variabili termodinamiche. Le NEF:
  - hanno intensità molto maggiore rispetto a quelle di equilibrio, un aumento di diversi ordini
    di grandezza che però è smorzato dall gravità e dalle dimensioni finite del campione a piccole
    numeri d'onda. La gravità influenza molto le NEF
  - sono sempre a lungo range, non solo nelle vicinanze del punto critico
Le NEF presentano un'ampiezza quadratica media con un comportamento a legge di potenza del tipo q^-4,
dove q è il vettor d'onda, mentre a piccoli vettori d'onda la gravità fa saturare questa quantità a
un valore costante. In assenza di gravità, l'unico effetto fisico che impedisce la divergenza delle
NEF a piccoli vettori d'onda è la dimensione finita del campione. 
Il rilassamento delle fluttuazioni avviene per diffusione a grandi lunghezze d'onda, e per la spinta
di archimede a piccole (buoyancy). Anche il tempo di rilassamento ha un andamento a legge di potenza in 
q^-4, con lo stesso q di roll-off. 
La fisica dietro alle NEF è stata gradualmente sviluppata recentemente. Esse sono fortemente legate alle
proprietà di trasporto del fluido. Per questo motivo, analizzando le NEF si possono ricavare simultaneamente
tutti i coefficienti di trasporto. Queste proprietà controllano i processi fisico-chimici che avvengono
all'interno del sistema e, di conseguenza, l'osservazione delle NEF è particolarmente adatta a indagare
la competizione tra i differenti meccanismi (diffusione, spinta idrostatica, confinamento...) che contri-
buiscono alla dinamica del sistema. In questo senso le NEF possono essere pensate come uno strumento per
investigare le proprietà del fluido. Con una singola misura si possono ottenere una quantità di informazioni
sul sistema e ciò è vero in particolare per fluidi complessi, dove sono presenti una serie di meccanismi
e caratteristiche fisiche addizionali. 
Le NEF sono anche il seme dei moti convettivi in presenza di gravità. Quindi, studiarle in microgravità
può svelare i meccanismi che stanno dietro all'inizio della convezione senza l'effetto mascherante della
convezione stessa. 

L'ESPERIMENTO NEUF-DIX 

Lo scopo dell'esperimento è l'investigazione delle NEF associate alla diffusione in fluidi complessi.
Gli obiettivi comprendono sia la comprensione fondamentale delle NEF, sia lo sviluppo di strumenti 
diagnostici basati sulle fluttuazioni. È desiderabile approfondire la comprensione delle NEF in 
condizioni non ideali, ovvero alte concentrazioni, grandi gradienti, e in processi dipendenti dal 
tempo. I modelli attuali si basano su idrodinamica linearizzata, ma i termini di secondo ordine 
sono interesanti, ad esempio danno luogo a effetti non lineari come le forze di Casimir di non
equilibrio. I liquidi complessi sono il sistema ideale per studiare le NEF, non solo perché sono
rilevanti in molti processi naturali e tecnologici, ma anche perché permettono di tunare la dimensione
microscopica del sistema e il range dell'interazione, insieme alle scale temporali tipiche di diffusione.
Uno dei modi migliori per investigare i liquidi complessi è lo scattering di luce in near field, che permette
di operare a piccoli vettori d'onda necessari per le NEF. (inoltre la compattezza è importante nello spazio, 
aggiungo io). 
Dato che l'ampiezza e il tempo di rilassamento delle NEF dipendono dal coefficiente di Soret (legato alla
termodiffusione) e dal coefficiente di diffusione, le NEF potrebbero essere utili per una misura rapida dei
coefficienti di trasporto di una miscela multicomponente. Quindi un altro scopo dell'esperimento è mettere
a punto metodi diagnostici basati sulle NEF per misurare le proprietà termo-fisiche di miscele complesse.

FORZE DI CASIMIR IN EQUILIBRIO E FUORI DALL'EQUILIBRIO

Quando le fluttuazioni sono long range spazialmente, la loro intensità è influenzata dalla presenza dei bordi.
Allo stesso modo, la presenza di fluttuazioni long range influenza i bordi stessi, inducendo delle forze su
di essi, chiamate forze di Casimir, o effetto Casimir. Quando un fluido o una miscela fluida è in equilibrio,
si hanno fluttuazioni long range solo nelle vicinanze del punto critico. Comunque, le forze di Casimir di un
sistema in equilibrio sono molto deboli. Si è, invece, visto che le fluttuazioni termiche in fluidi in stati
steady di non equilibrio sono anomalmente grandi e long range. Il caso più studiato è quello di una miscela 
binaria nella quale un gradiente di temperatura induce un gradiente di concentrazione stazionario tramite 
termodiffusione (effetto Soret). L'intensità, in questo caso, delle NEF è data dalla legge di potenza q^-4 di
prima, che nello spazio reale, corrisponde, a piccole q, ad avere una lunghezza di correlazione che copre tutta
l'estensione spaziale del sistema. NOTA: questo risultato è valido in generale non solo vicino a un punto critico.
È stato previsto, come conseguenza di ciò, un nuovo tipo di effetto Casimir di non equilibrio che dovrebbe essere 
molto più intenso di quello di non equilibrio. L'esperimento nello spazio con lo shadowgraph  è perfetto per poter 
verificare sperimentalmente l'esistenza di un tale effetto (ancora non fatto), probabilmente utilizzando particelle
colloidali come sonde. 

FLUIDI COMPLESSI

È già stato fatto molto lavoro sulle NEF in miscele binarie. Tuttavia, in natura, si incontrano molti sistemi multi-
componenti e/o contenenti un numero significativo di componenti dal diverso peso e dimensione molecolare. Gli studi
sperimentali sulle NEF si sono concentrati soprattutto su soluzioni diluite di polimeri e soluzioni diluite di 
colloidi. Solo recentemente ci si sta spostando su soluzioni concentrate di colloidi. Aumentare il numero di costituenti
aumenta notevolmente le difficoltà teoriche e sperimentali. Per cui i sistemi ternari sono una scelta naturale per
il prossimo step. Essi mostrano già caratteristiche tipiche di miscele multicomponenti: cross-diffusion, barriere di 
diffusione, o diffusione osmotica. Allo stesso tempo rimangono maneggiabili, e il numero di coefficienti di diffusione
rimane piccolo (4). L'investigazione di miscele liquide contenenti polimeri è anche molto interessante a alte 
concentrazioni di polimeri vicino alla transizione vetrosa, dove ci si aspetta che l'effetto dell'entanglement, e del
rallentamento della dinamica influenzino le NEF.

NEF in una miscela ternaria che include un polimero

...varie cose...

TECNICA SPERIMENTALE

Le tecnivhe migliori per studiare fluttuazioni in campioni dielettrici trasparenti alla scala mesoscopica sono le tecniche
di scattering a piccolo angolo. Le disomogeneità locali di densità sono associate a variazioni locali dell'indice di 
rifrazione che scatterano la luce incidente nello spazio circostante. Per cui si potrebbe usare la solita scattering far 
field per studiare le fluttuazioni. Tuttavia, nel caso di fluttuazioni long ranged, le variazioni sono su scale spaziali
grandi, e la luce scatterata a piccoli angoli è influenzata dal raggio trasmesso. Per risolvere questa e altre difficoltà, 
le tecniche etherodyne near field. La figura di diffrazione raccolta a piccola distanza dal campione permette di ricostruire
il fattore di struttura del campione. L'idea è usare lo stesso sensore per misurare il raggio trasmesso e la luce diffusa, 
misurando la figura di interferenza. Vedi formula per l'intensità. Vantaggi:
  - high sensitivity: il segnale è proporzionale al campo scatterato
  - self-referencing: misura assoluta
  - allineamento ottico semplice
  - misura simultaneamente statica e dinamica (perchè)?

---------------------------------------------------------------------------------------------------------------------------

FRACTAL FRONTS OF DIFFUSION IN MICROGRAVITY

Riporta i risultati sperimentali ottenuti con l'esperimento FOTON-M3. Si trova che durante un processo di diffusione una 
soluzione di polimero diluita mostra fluttuazioni di concentrazione scale-invariant con dimensioni fino a millimetri e tempi
di rilassamento fino a 1000 s. L'unico limite all'andamento scale-free è dato dalle dimensioni del campione.

I fronti di diffusione in miscele di liquidi differenti dovrebbero teoricamente presentare una struttura frattale, che però
sulla terra è soffocata dalla presenza della gravità, che causa la spinta di archimede. 
L'ampiezza quadratica media delle NEF nello spazio è significativamente maggiore che sulla Terra.

KINETICS OF GROWTH OF NEF DURING THERMODIFFUSION IN A POLYMER SOLUTION

la crescita delle NEF su scala mesoscopica è direttamente collegata allo sviluppo del profilo di concentrazione su scala
macroscopica. 

----------------------------------------------------------------------------------------------------------------------------

Come ottenere le NEF? due modi: free-diffusion (isoterma) e con gradiente (non isoterma)

Free-diffusion: ho due sostanze miscelate, se mi metto vicino al punto critico, "bucando la curva di coesistenza" da sopra,
avrò separazione, dove la separazione è data dalla gravità: un fluido è più denso dell'altro e così via. La separazione non sarà
nettissima comunque c'è. POi buco la curva di coesistenza, andando sopra, per cui il fluido vuole formare una fase omogenea, si formano
delle fluttuazioni all'interfaccia (in una zona attorno all'interfaccia). Con questo metodo però serve la gravità

Con il gradiente di temperatura che causa un gradiente di concentrazione. side effects: ho anche fluttuazioni di temperatura le 
quali causano fluttuazioni di densità. 


Qual è l'origine, la causa, delle NEF? Ci sono sempre, in un fluido con due fasi più o meno separate (ad esempio quando impongo un 
gradiente) delle fluttuazioni (locali) di velocità. La componente verticale di queste fluttuazioi sposta volumi di fluido di un tipo
in regioni di fluido dell'altro tipo, ed ecco le fluttuazioni. Queste poi diffondono. Se c'è la gravità però il volume di queste regioni 
non può essere troppo grosso, altrimenti vengono riportate subito in basso (o in alto) questo perchè c'è una dimensione di roll-off sopra
la quale la forza di massa(volume) ovvero la forza peso, vince sulla diffusione che invece dipende dalla superficie della regione.
In sostanza quel che conta è la durata di queste fluttuazioni.

TEMI DA APPROFONDIRE/RIPASSARE
- teorema fluttuazione-dissipazione
