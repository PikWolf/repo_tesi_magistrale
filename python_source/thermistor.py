from math import log

c1 = 0.0027798638171422336404358518686988
c2 = 0.00023740205744734894527153370376738
c3 = 0.000006593868949040509169169720559245
c4 = -0.00000059224388513967090414832511831716

R1 = float(input('Insert the resistance, in kohm: '))

T1 = (1.0 / (c1 + c2*log(R1) + c3*log(R1)*log(R1) + c4*log(R1)*log(R1)*log(R1))) - 273.15
print('The temperature is: ',T1)



