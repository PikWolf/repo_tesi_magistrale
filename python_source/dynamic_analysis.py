import image_processing as i
from numba import njit,prange
import sys
from tqdm import tqdm

def dictToFile(d,folder,q) :
  with open(folder+'/'+str(q)+'.txt','w') as f :
    f.write('# Structure function for q = '+str(q)+'\n# tau  f(q,tau)\n')
    for point in d :
      f.write(str(point[0])+'  '+str(point[1])+'\n')

##@njit(parallel=True)
def main() : 
  print('Reading the configuration file...')
# reading the configuration file
  with open(sys.argv[1],'r') as conf :
    folder   = conf.readline().split()[1]
    path     = conf.readline().split()[1]
    extremes = tuple(map(int,(conf.readline().split(maxsplit = 1)[1]).split(sep = ' ')))
    q_range  = tuple(map(int,(conf.readline().split(maxsplit = 1)[1]).split(sep = ' ')))
    n_av     = int(conf.readline().split()[1])
    t_range  = tuple(map(float,(conf.readline().split(maxsplit = 1)[1]).split(sep = ' ')))
    max_tau  = float(conf.readline().split()[1])
    tau_step = float(conf.readline().split()[1])

  # scan the folder and store the filenames
  ndict = i.namesDict(path)
  nlist = [k[1] for k in ndict.items()]
  mintau,maxtau = i.findMinMaxTau(ndict)
  print('mintau is ',mintau,' maxtau is ',maxtau)
  if mintau > maxtau :
    print('mintau higher than maxtau, i am going to stop')
    return 0
  # now, before analysing the data, it has to be cleaned
  back = i.backgroundPrecise(path,nlist,extremes)
  # ready for analysis
  for q in tqdm(range(q_range[0],q_range[1],1)) :
      d = i.structureFunction(path,ndict,n_av,t_range,q,mintau,max_tau,tau_step,back)
      dictToFile(d,folder,q)

if __name__ == "__main__" :
  main()
# tante cose da chiedere

