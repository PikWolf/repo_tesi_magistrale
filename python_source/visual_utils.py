import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import json
import bottleneck as bn
import pandas as pd

def plotT(filename) :
  """This function plots the temperature trend of the 
  measure, givent the temperature log file"""
  
  with open(filename,'r') as f :
    leng = sum(1 for l in f)
  data = np.zeros((leng-5,3))
  with open(filename,'r') as f :
    count = 0
    count1 = 0
    for l in f :
      if count in {0,1,2,leng-1,leng-2} :
        pass
      else :
        data[count1] = l.split()
        count1 = count1 +1
      count = count + 1
  print(data.shape)
  plt.plot(data[:,0],data[:,1:3],linestyle = '-',marker='.',label = 'T')
  plt.xlabel('time (s)')

def plotVar(filename) :
  with open(filename,'r') as f :
    data = [l.split() for l in f]
  dat = np.asarray(data)
  plt.plot(dat[:,0],dat[:,1])

def plotStructureFunctionTotal(filename) :
  fig = plt.figure()
  ax = fig.gca(projection='3d')
  plot = np.loadtxt(filename)
  taus = [x[0] for x in plot]
  z = np.array([1.0 for point in taus])
#  print(taus)
  ax.set_xlabel('taus')
  ax.set_ylabel('C')
  ax.set_zlabel('q')
  for n,func in enumerate(plot.transpose()[1:]) :
#    print(func.shape)
    ax.plot(taus,func,n*z,'-.')

def plotStructureFunctionTotalCsv(filename) :
  fig = plt.figure()
  plot = pd.read_csv(filename)
  for i in plot.columns :
    plot[i].plot()


def plotStructureFunctions(filename,q,mean = False) :
  plot = np.loadtxt(filename)
  taus = [x[0] for x in plot]
  plt.plot(taus,plot[:,q],'.',label = 'Structure function q = '+str(q) )
  plt.xlabel('Ritardo (s)')
  plt.legend()
  mv_av = bn.move_mean(plot[:,q],10)
  plt.plot(taus,mv_av)
  plt.legend(loc = 'upper right')

def plotVar(filename,n_av,DT) :
  with open(filename,'r') as f :
    var_dict = json.load(f)
  vd = {}  
  for k in var_dict : 
    vd[float(k)] = var_dict[k] 
  x = np.array(list(vd.keys()))
  y = np.array(list(vd.values()))
  plt.plot(x,y,'.',label = '\u0394T = '+str(DT)+  ' °C data')
  plt.xlabel('time (s)')
  plt.title('Variance')
  mv_av = bn.move_mean(y,n_av)
  plt.plot(x,mv_av,label = '\u0394T = '+str(DT)+  ' °C moving average (n = '+str(n_av)+' )')
  plt.legend()

def plotTranslation(filename,u) :
  with open(filename,'r') as f :
    var_dict = json.load(f)
  vd = {}  
  for k in var_dict : 
    vd[float(k)] = np.linalg.norm(var_dict[k])
  x = np.array(list(vd.keys()))
  y = np.array(list(vd.values()))
  plt.plot(x,y,'.-',label = 'upsamplig = ' +str(u))
  plt.xlabel('time (s)')
  plt.title('translation')
  plt.legend()
