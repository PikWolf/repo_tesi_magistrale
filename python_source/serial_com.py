# program to communicate with several serial instruments

# Wavelength Electronics Temperature controller LFI-3751

 # 'LOCAL'  : ('!10'+n+'253+000.00020').encode('ascii'), # revert to local mode
def checksum(b) :
  """This function takes a byte object as input and computes the xor
  checksum, needed to send the correct packet. Returns a string."""
  x = b[0]
  for i in range(1,len(b)) :
    x = x^b[i]
  return hex(x)[2:]

def only_value(line) :
  """This function takes as input a controller answer packet, and 
  returns only the value of interest."""
  return float(line.decode('ascii')[9:17])

def T(s, n) :
  """This functin takes as input a serial port object, s, assumed open
  and the number, n, that identifies the controller (1 or 2 in my 
  case). It returns the actual temperature, measured by the sensor."""
  c = checksum(('!10'+n+'101+000.000').encode('ascii')) 
  s.write(('!10'+n+'101+000.000'+c).encode('ascii'))
  return only_value(s.readline())

def set_T(s, n, T) :
  """This functin takes as input a serial port object, s, assumed open
  and the number, n, that identifies the controller (1 or 2 in my 
  case). It also take a temperature value T. T must be a string made of
  8 digits (e.g. '-012.100'). It sets the T setpoint."""
  # maybe check for current T limits and T format?
  c = checksum(('!10'+n+'203'+T).encode('ascii'))
  s.write(('!10'+n+'203'+T+c).encode('ascii'))
  s.readline()
  #control for error or read the setpoint?

def local(s,n) :
  """This function reverts the n controller to local mode"""
  c = checksum(('!10'+n+'253+000.000').encode('ascii'))
  s.write(('!10'+n+'253+000.000'+c).encode('ascii'))
  return s.readline()
  # check opennes? write exceptions?

## some functions for the output
def out_on(s,n) :
  """This function enables the output current."""
  c = checksum(('!10'+n+'251+000.001').encode('ascii'))
  s.write(('!10'+n+'251+000.001'+c).encode('ascii'))
  s.readline()

def out_off(s,n) :
  """This function disables the output current."""
  c = checksum(('!10'+n+'251+000.000').encode('ascii'))
  s.write(('!10'+n+'251+000.000'+c).encode('ascii'))
  s.readline()

## PID control
def set_P(s,n,P) :
  """This function sets the P parameter. es P ='030.000'."""
  c = checksum(('!10'+n+'210+'+P).encode('ascii'))
  s.write(('!10'+n+'210+'+P+c).encode('ascii'))
  s.readline()

def set_I(s,n,I) :
  """This function sets the I parameter. es I ='030.000'."""
  c = checksum(('!10'+n+'211+'+I).encode('ascii'))
  s.write(('!10'+n+'211+'+I+c).encode('ascii'))
  s.readline()

def set_D(s,n,D) :
  """This function sets the D parameter. es D ='030.000'."""
  c = checksum(('!10'+n+'212+'+D).encode('ascii'))
  s.write(('!10'+n+'212+'+D+c).encode('ascii'))
  s.readline()
