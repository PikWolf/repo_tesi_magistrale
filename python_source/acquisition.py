# In this file there are some routines to do image 
# acquisition with the NI PCI-1407 interface.

import ctypes as c
import nivision as ni
import time

# load tyhe imaq DLL
imaq = c.WinDLL('C:\Windows\SysWOW64\imaq.dll')
# Create a void image
image = ni.imaqCreateImage(ni.IMAQ_IMAGE_U8)

def open() :
  """This function open the interface and begins the session"""
  ID = c.c_long(0)
  SID = c.c_long(0)
  if imaq.imgInterfaceOpen(c.c_char_p('img0'.encode('utf-8')),
          c.addressof(ID)) == 0  and \
                  imaq.imgSessionOpen(ID,c.addressof(SID)) == 0 :
    return ID.value,SID.value
  else : return False

def close(ID,SID) :
  """This function closes the interface and session."""
  imaq.imgClose(ID,1)
  imaq.imgClose(SID,1)

def snap(SID,img) :
  """This function takes a snap and returns the image."""
  return ni.imaqSnap(SID,img,ni.IMAQ_NO_RECT)

def toTiff(imageP,filename) :
  """This function takes an image reference and save the image 
  to a .tif file."""
  ni.imaqWriteTIFFFile(imageP,(filename.encode('utf-8')))

def toBmp(imageP,filename) :
  """This function takes an image reference and save the image 
  to a .bmp file."""
  ni.imaqWriteBMPFile(imageP,(filename.encode('utf-8')),False,None)

def setupGrab(SID) :
  """This function setups the grab function."""
  ni.imaqSetupGrab(SID,ni.IMAQ_NO_RECT)

def grab(SID) :
  """This function grabs an image from the buffer of continuosly 
  acquired images."""
  return ni.imaqGrab(SID,None,True)

def stop(SID) : 
  """This function stops the acquisition."""
  ni.imaqStopAcquisition(SID)


##############################################################
## Test code #################################################
##############################################################

if __name__ == "__main__" :
  measure_time = 30
  t = 0

  # name of the interface
  name = c.c_char_p('img0'.encode('utf-8'))

  # interface open
  ID = c.c_long(0)
  if imaq.imgInterfaceOpen(name,c.addressof(ID)) == 0 :
    # session open
    SID = c.c_long(0)
    if imaq.imgSessionOpen(ID,c.addressof(SID)) == 0 :
      # acquisition loop
      start = time.time()
      while t < measure_time :
        t = time.time() - start
        snap = ni.imaqSnap(SID.value,None,ni.IMAQ_NO_RECT)
        ni.imaqWriteTIFFFile(snap,('snap'+str(t)+'.tif').encode('utf-8'))
        time.sleep(1)
      imaq.imgClose(SID.value,1)
