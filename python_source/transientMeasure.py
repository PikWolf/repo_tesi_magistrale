# program that control the temperature of the two plates and takes
# pictures of the sample.
import sys
import time
import serial as ser
import serial_com as s
import signal
import acquisition as a
import nivision as ni
import ctypes as c


# parsing the input parameters
print('Reading the configuration file...')
with open(sys.argv[1],'r') as conf :
  measure_time = int(conf.readline().split()[1])
  filename = conf.readline().split()[1]
  T1 = conf.readline().split()[1]
  T2 = conf.readline().split()[1]
  t1 = conf.readline().split()[1]
  t2 = conf.readline().split()[1]
  P1 = conf.readline().split()[1]
  P2 = conf.readline().split()[1]
  I1 = conf.readline().split()[1]
  I2 = conf.readline().split()[1]
  D1 = conf.readline().split()[1]
  D2 = conf.readline().split()[1]
  tm = float(conf.readline().split()[1])
  pr = conf.readline().split()[1]

print(tm)
    
# opening the serial port with correct parameters. Here i 
# assume we are using a LFI-3751 controller and the OS is 
# Windows, and the port is COM1. Also, opening output file
# for temperature log.
with ser.Serial('COM1', 19200, timeout = 2, 
                parity = ser.PARITY_NONE, xonxoff = True) as tc,\
                open((pr+'T/'+filename),'a') as f:
  # write some info in the file
  f.write('Starting time:  '+str(time.ctime())+'\n')
  f.write('P1 = '+P1+', I1 = '+I1+', D1 = '+D1+'\n')
  f.write('P2 = '+P2+', I2 = '+I2+', D2 = '+D2+'\n')
  
  # initialize some useful variables
  t = 0

  ### set the image acquisition interface ###
  # open the interface and begin the session
  ID,SID = a.open()
  
  # signal handler
  def handler(signum,frame,fileob = f,se = tc,timei = t,i_d = ID,sid = SID) :
    # write some info to the file
    fileob.write('Measure interrupted. Total measuring time:  '+str(timei)+' s.\n')
    fileob.write('Ending time:  '+str(time.ctime())+'\n')
    # disable output
    s.out_off(se,'1')
    s.out_off(se,'2')
    # revert to local mode
    s.local(se,'1')
    s.local(se,'2')
    sys.exit('Execution interrupted')
    # stop the acquisition
    a.stop(sid)
    # close the acquisition interface and session
    a.close(i_d,sid)

  ### prepare the controllers ###
  # set temperature and current limits
  # set the thermistor pair values
  # set PID parameters
  s.set_P(tc,'1',P1)
  s.set_P(tc,'2',P2)
  s.set_I(tc,'1',I1)
  s.set_I(tc,'2',I2) 
  s.set_D(tc,'1',D1)
  s.set_D(tc,'2',D2)

  
  ########### measure loop #######################
  # loop that monitor T, write it to the log file, 
  # acquire the images from the camera and saves 
  # them to files.

  # set the signal handler
  signal.signal(signal.SIGINT,handler)
  # setup the grab operation
  a.setupGrab(SID)

  start = time.time()
  count = 0
  
  # first: impose a tiny gradient for an hour and measure (but with
  # low frequency to have the background
  # set the T setpoint
  s.set_T(tc,'1',t1)
  s.set_T(tc,'2',t2)
  s.out_on(tc,'1')
  s.out_on(tc,'2')

  while t < 3600 :
    count = count + 1
    T_1 = str(s.T(tc,'1'))
    T_2 = str(s.T(tc,'2'))
    t = time.time() - start
    te = str(round(t,3))
    if count % 5 == 0 :
      print(count)
      f.write(te+'  '+T_1+'  '+T_2+'\n')
    
    # grab an image from the buffer
    img = a.grab(SID)
    # saving the image to a file
    a.toBmp(img,(pr+str(count)+'_'+te+'_'+T_1+'_'+T_2+'.bmp'))
    time.sleep(1)
  
  # second: switch on the gradient and measure what happens
  # enable output
  # set the T setpoint
  s.set_T(tc,'1',T1)
  s.set_T(tc,'2',T2)
  s.out_on(tc,'1')
  s.out_on(tc,'2')

  while t < measure_time + 3600 :
    count = count + 1
    T_1 = str(s.T(tc,'1'))
    T_2 = str(s.T(tc,'2'))
    t = time.time() - start
    te = str(round(t,3))
    if count % 5 == 0 :
      print(count)
      f.write(te+'  '+T_1+'  '+T_2+'\n')
    
    # grab an image from the buffer
    img = a.grab(SID)
    # saving the image to a file
    a.toBmp(img,(pr+str(count)+'_'+te+'_'+T_1+'_'+T_2+'.bmp'))
    time.sleep(tm)
 
  # Stop the acquisition
  a.stop(SID)
  # write some info to the file
  f.write('Measure completed. Total measuring time:  '+str(t)+' s.\n')
  f.write('Ending time:  '+str(time.ctime())+'\n')
  # disable output
  s.out_off(tc,'1')
  s.out_off(tc,'2')
  # revert to local mode
  s.local(tc,'1')
  s.local(tc,'2')
  # close the acquisition interface and session
  a.close(ID,SID)
