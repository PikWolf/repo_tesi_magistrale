from processing_serialize import Fit
import pandas as pd
import numpy as np 

filename = input('Type the full path of the file containing the results: ')
start_time = filename.split(sep = '_')[-4]
end_time = filename.split(sep ='_')[-3]
image_step = filename.split(sep = '_')[-2]
tau_bin_width = filename.split(sep = '_')[-1]
path_list = filename.split(sep = '/')[1:-1]
path = '/'.join(path_list)

fits = Fit(filename, guess = np.array([20000.,100.,500.]), extremes = ([1.,0.1,1.],[100000.,100000.,10000.]))

fits.to_csv('/'+path+'/'+'fits_'+start_time+'_'+end_time+'_'+image_step+'_'+tau_bin_width+'.csv')
# static spectrum
Sq = fits.A
Sq.to_csv('/'+path+'/'+'sq_'+start_time+'_'+end_time+'_'+image_step+'_'+tau_bin_width+'.csv')

# Stochastic noise
Bq = fits.B
Bq.to_csv('/'+path+'/'+'bq_'+start_time+'_'+end_time+'_'+image_step+'_'+tau_bin_width+'.csv')

# Taus
Taus = fits.Tau
Taus.to_csv('/'+path+'/'+'taus_'+start_time+'_'+end_time+'_'+image_step+'_'+tau_bin_width+'.csv')
