## A collection of functions to analize the information
## contained in the images.
#cython: language_level = 3
import os
import numpy as np
import matplotlib.pyplot as plt
from natsort import natsorted
from tqdm import tqdm
from collections import OrderedDict as od
from bisect import bisect_left
import cv2
from collections import Counter 
from copy import deepcopy
from joblib import Parallel,delayed,Memory 
import pickle
from numba import njit
import json
from skimage.feature import register_translation as rt
from skimage.filters import gaussian as gs

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
#####################################################################
################ I/O and basic preprocessing #######################

def take_closest(myArray, myNumber):
  """
  Assumes myList is sorted. Returns closest value to myNumber.

  If two numbers are equally close, return the smallest number.
  """
  myList = list(myArray)
  pos = bisect_left(myList, myNumber)
  if pos == 0:
    return myList[0]
  if pos == len(myList):
    return myList[-1]
  before = myList[pos - 1]
  after = myList[pos]
  if after - myNumber < myNumber - before:
   return after
  else:
   return before
def openImage(name,bw = 0) :
  return (cv2.imread(name,bw)).astype(np.float64)

def openImage8(name,bw = 0) :
  return cv2.imread(name,bw)

def normalize(imarray) :
  """This function takes a bidimensional ndarray as input and 
  normalizes it by dividing it by its spatial average.""" 
  return imarray/np.average(imarray)

def insideFillBufferCu(b,ndict,imbuffer) : 
  imbuffer[b] = openImage8(ndict[b])

def fillBufferCu(path,ndict,timesArray,imbuffer,image_step) :
  """This function fill an ordered dict with the subsect of the imagese to 
  analyse. So it needs the image step and the time extremes to consider."""

  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBufferCu)(b,ndict,imbuffer) for b in tqdm(timesArray))
  return imbuffer


@njit
def cutImage(imarray) : # domanda: come fai a essere sicuro che l'immagine è centrata? è importante? aggiungere nu argomento: posizione del centro?
  """This function takes a 576x768 array and cuts a little square
  inside the image."""
  return imarray[159:415,255:511]

def insideCutLoop(pathw,ndict,imbuffer,h) : 
  cv2.imwrite((pathw+ndict[h]),cutImage(imbuffer[h]))
  

def cutImagesA(path,ndict,timesArray) :
  """cuts all the images contained in path and stores them in a folder
  named cuts in the same folder"""
  os.mkdir(path+'/cuts')
  os.chdir(path)
  pathw = path+'/cuts/'
  imbuffer = od()
  for step in tqdm(range(len(nArray)//1000)) :
    times_ar = timesArray[step*1000:step*1000+1000]
    imbuffer = fillBufferCu(path,ndict,times_ar,imbuffer,image_step = 1)
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideCutLoop)(pathw,ndict,imbuffer,h) for h in tqdm(times_ar) )
  remains = timesArray[step*1000+1000:]
  imbuffer = fillBufferCu(path,ndict,remains,imbuffer,image_step = 1)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideCutLoop)(pathw,ndict,imbuffer,h) for h in tqdm(imbuffer) )

def namesList(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and (entry.name.endswith('bmp') or entry.name.endswith('tif')) :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return n_list

def namesArray(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and entry.name.endswith('bmp') :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return np.asarray(n_list)

def namesDict(path) :
  """This function scans the folder given by path and returns
  a dictionary containing the pairs (key : time , value : picture)."""
  
  with os.scandir(path) as fol :
    n_dict = { float(entry.name.split('_')[1]) : entry.name  for entry in fol if (entry.is_file() and (entry.name.endswith('bmp') or entry.name.endswith('tif'))) }
    n_dict_sorted = od(sorted(n_dict.items(),key = lambda x : x[0]))
  
  return n_dict_sorted

def timesArray(ndict) :
  return np.array([x[0] for x in ndict.items()])

def pickTimes(timesArray,time_extremes) :
  sx_index = np.where(timesArray == time_extremes[0])[0][0]
  dx_index = np.where(timesArray == time_extremes[1])[0][0]
  return timesArray[sx_index:dx_index:image_step]

def insideFillBuffer(b,ndict,imbuffer) : 
  imbuffer[b] = openImage(ndict[b])

def fillBuffer(path,ndict,timesArray,imbuffer,image_step) :
  """This function fill an ordered dict with the subsect of the imagese to 
  analyse. So it needs the image step and the time extremes to consider."""

  os.chdir(path+'/cuts')

  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBuffer)(b,ndict,imbuffer) for b in tqdm(timesArray))
  return imbuffer

def insideFillBufferTr(b,ndict,imbuffer) : 
  imbuffer[b] = (openImage(ndict[b]))[223:351,319:447]

def fillBufferTr(path,ndict,timesArray,imbuffer,image_step) :
  os.chdir(path)
  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBufferTr)(b,ndict,imbuffer) for b in tqdm(timesArray[::image_step]))
  return imbuffer

## for the gaussian blur filter
def insideFillBufferGs(b,ndict,imbuffer) : 
  imbuffer[b] = (openImage(ndict[b]))

def fillBufferGs(path,ndict,timesArray,imbuffer,image_step) :
  os.chdir(path)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBufferGs)(b,ndict,imbuffer) for b in tqdm(timesArray[::image_step]))
  return imbuffer
######
def timesDiffs(ndict) :
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  return diffs

def findMinMaxTau(ndict) :#da sostituire magari con un tauDistr che trova la distribuzione dei tau e trova il picco, per stabilire come binnare i tau
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  print(diffs.index(max(diffs)))
  return max(diffs),round(float(keys[-1]),ndigits=2)

def binArrayCenters(bins) :
  return np.asarray([(bins[ie]+bins[ie+1])/2 for ie in range(len(bins)-1)])

#####################################################################
################## image manipulation ###############################

def fft(imagearray) : # capire con il prof se questa cosa ha senso
  # compute the fft
  fft1 = np.fft.fft2(imagearray)
  # move the origin to the center of the image
  fft1 = np.fft.fftshift(fft1)
  # reduce the dynamic range of the magnitude (absolute value)
  fft1 = np.log10(np.abs(fft1))
  # compute the min and max value for normalization
  ma = np.nanmax(fft1[np.isfinite(fft1)])
  mi = np.nanmin(fft1[np.isfinite(fft1)])
  #print(mi,ma)
  # normalize
  fft1 = (fft1-mi)/(ma-mi)*255
  #print(fft1.shape)
  return (np.rint(fft1)).astype(np.int8)

def insideFft(ini) :
  imbuffer[ini] = fft(imbuffer[ini])


def folderFft(imbuffer) :
  """This function computes the fourier transform of the images
  contained in path. It then saves them in a folder called fft/"""
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFft)(ini) for ini in tqdm(imbuffer))


y,x = np.indices((256,256))
r = np.sqrt((x-127)**2+(y-127)**2)
r = r.astype(np.int)
ra = r.ravel()
nr = np.bincount(ra)

def azimuthalAverageFast(immarray) :
  """This function basically do the same as the other azimuthal average,
  but here the r matrix is passed as an argument, so that can be computed 
  only once"""
  tbin = np.bincount(ra,immarray.ravel())
  return tbin/nr

def insideGaussian(ndict,pathw,imbuffer,correction,key) :
  cv2.imwrite(ndict[key],gs(imbuffer[key],sigma = 5)) 

def GaussianBlur(path,ndict,timesArray,time_stop,image_step = 10) :# aggiungere anche un timestep per velocizzare? magari non è necessario proprio farle tutte 
  """This function takes the images in the folder and applies a gaussian filter to get 
  rid of the faster noise."""
  correction = od()
  os.mkdir(path+'/gaussian_filtered')
  pathw = path+'/gaussian_filtered'
  imbuffer = od()
  times_ar = timesArray[::image_step]
  times_ar = times_ar[times_ar<time_stop]
  for step in tqdm(range(max(1,len(times_ar)//1000))) :
    imbuffer = fillBufferGs(path,ndict,times_ar[step*1000:step*1000+1000],imbuffer,image_step = 1)
    os.chdir(pathw)
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideGaussian)(ndict,pathw,imbuffer,correction,key) for key in tqdm(imbuffer)) 
  remains = times_ar[step*1000+1000:]
  imbuffer = fillBufferGs(path,ndict,remains,imbuffer,image_step = 1)
  os.chdir(pathw)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideGaussian)(ndict,pathw,imbuffer,correction,key) for key in tqdm(imbuffer)) 

#####################################################################
################ Dynamic analysis ###################################

def insideBigLoop(im,k,k1,imbuffer,bins,results,average_array) :
  #print('type of im: ',type(im), ' tipe of im1: ',type(im1), ' key: ',key,' key1: ',key1)
  
  tau = np.digitize(k1 - k,bins) - 1
  dif  = imbuffer[k1]- im
  results[tau] = azimuthalAverageFast(np.abs(dif)**2)
  average_array[tau] = average_array[tau] + 1

def bigLoop(results,bins,imbuffer,timesArray,average_array) :
  timesArray1 = deepcopy(timesArray)
  co = 0
  for k in tqdm(timesArray) :
    co = co+1
    im = imbuffer[k]
      # for each images in the data compute the difference of the ffts of the next
      # images and it
    if len(ndict) > 0 :
      Parallel(n_jobs = 4,prefer = 'threads',require = 'sharedmem')(delayed(insideBigLoop)(im,k,k1,imbuffer,bins,results,average_array) for k1 in tqdm(timesArray1[co:]))

##################################################################
########################## statistical routines #####################
def averageImage(ndict,seconds) :
  avi = np.zeros((256,256))
  counter = 0
  for key in ndict :
    if key > seconds :
      break
    counter = counter +1
    avi = avi + openImage(ndict[key])
  print(counter,key)
  return avi/counter, counter

def insideVar(ai,key,ndict,vdict) :
  vdict[key] = np.sum((openImage(ndict[key])-ai)**2)

def folderVar(ndict,ai,number_of_images) :
  vdict = od()
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideVar)(ai,key,ndict,vdict) for key in tqdm(ndict))
  return vdict

def movingAverage(array, n=3) :
    ret = np.cumsum(array, dtype=np.float64)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

#####################################################################
################# charaterizing the traslation ######################
def insideTranslation(imbuffer,correction,key,im1,upsampling) :
  correction[key] = rt(imbuffer[key],im1,upsample_factor = upsampling)[0]

def translationCorrection(path,ndict,timesArray,time_stop,image_step = 10,upsampling = 1) :# aggiungere anche un timestep per velocizzare? magari non è necessario proprio farle tutte 
  """This function takes the images in the folder and computes the 
  time crosscorrelation function with respect to the first image, in 
  order to see how the whole image tralsates in the plane during the 
  transient."""
  correction = od()
  os.chdir(path)
  imbuffer = od()
  im1 = openImage(ndict[timesArray[0]])[223:351,319:447]
  times_ar = timesArray[::image_step]
  times_ar = times_ar[times_ar<time_stop]
  for step in tqdm(range(max(1,len(times_ar)//1000))) :
    imbuffer = fillBufferTr(path,ndict,times_ar[step*1000:step*1000+1000],imbuffer,image_step = 1)
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideTranslation)(imbuffer,correction,key,im1,upsampling) for key in tqdm(imbuffer)) 
  remains = times_ar[step*1000+1000:]
  imbuffer = fillBufferTr(path,ndict,remains,imbuffer,image_step = 1)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideTranslation)(imbuffer,correction,key,im1,upsampling) for key in tqdm(imbuffer)) 

  with open(path+'/traslation'+str(upsampling)+'.json','w') as fil :
    json.dump(correction,fil,cls = NumpyEncoder)


#####################################################################
########################### MAIN ####################################
#####################################################################

if __name__ == "__main__" :

  path = input('Insert the path of the folder containing\nthe images to be processed:')
  nArray = namesArray(path)
  nArray1 = deepcopy(nArray)
  ndict = namesDict(path)
  timesArray = timesArray(ndict)
  print('Making the inventory:')
  print('  - number of images: ',len(nArray))
  total_time = float(nArray[-1].split(sep='_')[1])
  print('  - total duration of the measure (s): ',total_time)
  timeDiffs = Counter(timesDiffs(ndict))
  most_freq_diff = list(od(sorted(timeDiffs.items(),key = lambda x : x[1],reverse = True)).keys())[0]
  print('  - most frequent time difference (s): ',most_freq_diff)
  width = float(input('If you want a coarser binning, insert a width greater than the most\nfrequent time difference: '))
  tau_bin_width = max(round(most_freq_diff,ndigits = 4),width)
  print('  - tau bin width: ',tau_bin_width)
  bin_number = int(total_time//tau_bin_width)
  bins =[0]+[tau_bin_width*k for k in range(1,bin_number+1)]
  print('bins ', len(bins))
  print(' - bin number: ',bin_number)
  q_number = 182 # controllare che sia effettivamente così
  results = np.zeros((bin_number,q_number))
  average_array = np.zeros(bin_number) # to store the number of differences with that tau value
  flag = input('Insert what type of analysis you want. Accepted answers:\n - cut\n - dynamic\n - variance\n')
  

  if flag == 'cut' :
    print('selected: cut')
    # cut the images
    print('Cutting the images...')
    cutImagesA(path,ndict,timesArray)
    print('Done.')

  elif flag == 'dynamic' :
    print('selected: dynamic')
    image_step = int(input('Insert the image_step, it must be an integer: '))
    time_inf = float(input('Insert the beginning time of the time window you want to consider: '))
    time_sup = float(input('Insert the ending time of the time window you want to consider: '))
    time_range = (take_closest(timesArray,time_inf),take_closest(timesArray,time_sup))
    print('time range selected: ['+str(time_range[0])+','+str(time_range[1])+']')
    print('filling the image buffer...')
    timesArray = pickTimes(timesArray,time_range)
    imbuffer = od()
    imbuffer = fillBuffer(path,ndict,timesArray,imbuffer,image_step)

    # compute ffts of the images in the folder and save them in the folder: fft.
    print('Computing the fft of the cut images. It may take a while...')
    folderFft(imbuffer)
   
    # here begins the main double loop over the images
    print('Now the big loop')
    bigLoop(results,bins,imbuffer,timesArray,average_array)
    
    for rowi in range(len(results)) :
      if average_array[rowi] != 0 :
        results[rowi] = results[rowi]/average_array[rowi]
    print(results ) 
    bincenters = binArrayCenters(bins).reshape(bin_number,1)
    results = np.hstack((bincenters,results))
   
    with open(path+'/results.txt','w') as fi :
      np.savetxt(fi,results)
      # domanda, ma la fft come la faccio io va bene?
      # non solo: come tenere conto delle medie?
    
  elif flag == 'variance' :
    print('selected: variance')
    os.chdir(path+'/cuts')
    seconds = float(input('Insert the seconds of measure to consider for averaging: '))
    # compute the average image form the first 5 minutes
    ai,number  = averageImage(ndict,seconds)
    # compute the variance array
    number_of_images = len(nArray) - number 
    vdict = folderVar(ndict,ai,number_of_images)
    with open(path+'/variance.json','w') as fil :
      json.dump(vdict,fil)
      # fare media mobile e magari sottrarre il primo punto (ma non ho capito bene questa cosa)
