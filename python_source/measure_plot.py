# program that control the temperature of the two plates.
import sys
import time
import serial as ser
import serial_com as s
import matplotlib.pyplot as plt
import signal
import acquisition as a
import nivision as ni


# parsing the input parameters
with open(sys.argv[1],'r') as conf :
  measure_time = int(conf.readline().split()[1])
  filename = conf.readline().split()[1]
  T1 = conf.readline().split()[1]
  T2 = conf.readline().split()[1]
  P1 = conf.readline().split()[1]
  P2 = conf.readline().split()[1]
  I1 = conf.readline().split()[1]
  I2 = conf.readline().split()[1]
  D1 = conf.readline().split()[1]
  D2 = conf.readline().split()[1]
  tm = float(conf.readline().split()[1])
  pr = conf.readline().split()[1]

# opening the serial port with correct parameters. Here i 
# assume we are using a LFI-3751 controller and the OS is 
# Windows, and the port is COM1. Maybe I 
# can generalize this.
with ser.Serial('COM1', 19200, timeout = 2, 
                parity = ser.PARITY_NONE, xonxoff = True) as tc,\
                open((pr+'T/'+filename),'a+') as f:
  # write some info in the file
  f.write('Starting time:  '+str(time.ctime())+'\n')
  f.write('P1 = '+P1+', I1 = '+I1+', D1 = '+D1+'\n')
  f.write('P2 = '+P2+', I2 = '+I2+', D2 = '+D2+'\n')

  t = 0
  img = ni.imaqCreateImage(ni.IMAQ_IMAGE_U8)
  # signal handler
  def handler(signum,frame,fileob = f,se = tc,timei = t) :
    # write some info to the file
    fileob.write('Measure interrupted. Total measuring time:  '+str(timei)+' s.\n')
    fileob.write('Ending time:  '+str(time.ctime())+'\n')
    # disable output
    s.out_off(se,'1')
    s.out_off(se,'2')
    # revert to local mode
    s.local(se,'1')
    s.local(se,'2')
    sys.exit('Execution interrupted')

  ###############################

  ### prepare the controllers ###
  # set temperature and current limits
  # set the thermistor pair values
  # set PID parameters
  s.set_P(tc,'1',P1)
  s.set_P(tc,'2',P2)
  s.set_I(tc,'1',I1)
  s.set_I(tc,'2',I2) 
  s.set_D(tc,'1',D1)
  s.set_D(tc,'2',D2)
  # set the T setpoint
  s.set_T(tc,'1',T1)
  s.set_T(tc,'2',T2)
  ###############################

  ### prepare the plot ##########
  # set interactive mode 
  plt.ion()
  # instantiate the figure and the axes.
  fig,(ax2,ax1) = plt.subplots(2,1)
  ax1.set_title('temperature A')
  ax1.axhline(y=float(T1))
  ax1.set_xlabel('time (s)')
  ax2.axhline(y=float(T2))
  ax2.set_title('temperature B')
  plt.show()

  ### set the image acquisition interface ###
  # open the interface and begin the session
  ID,SID = a.open()

  # enable output
  s.out_on(tc,'1')
  s.out_on(tc,'2')
  # loop that monitor T and listen for errors. It plots the temperature
  # (if you want) and takes a snap of the sample.
  start = time.time()
  count = 0
  # set the signal handler
  signal.signal(signal.SIGINT,handler)
  while t < measure_time :
    count = count + 1
    pos = f.tell()
    t = time.time() - start
    te = str(round(t,3))
    T_1 = str(s.T(tc,'1'))
    T_2 = str(s.T(tc,'2'))
    f.write(te+'  '+T_1+'  '+T_2+'\n')
    
    # taking the snap
    print(img)
    img = a.snap(SID,None)
    print(img)
    a.toTiff(img,pr+str(count)+'_'+te+'_'+T_1+'_'+T_2+'.tiff')

    # plot part
    f.seek(pos)
    line = f.readline().split()
    if t>100 :
      ax1.set_xlim(right = (t + 0.3))
      ax2.set_xlim(right = (t + 0.3))
      ax1.set_xlim(left = (t - 99))  
      ax2.set_xlim(left = (t - 99))  
    ax1.plot(t,float(line[1]),'b.',markersize=4)
    ax2.plot(t,float(line[2]),'r.',markersize=4)
    time.sleep(tm)
    plt.pause(0.00001)
  
  # write some info to the file
  f.write('Measure completed. Total measuring time:  '+str(t)+' s.\n')
  f.write('Ending time:  '+str(time.ctime())+'\n')
  # disable output
  s.out_off(tc,'1')
  s.out_off(tc,'2')
  # revert to local mode
  s.local(tc,'1')
  s.local(tc,'2')
  # close the acquisition interface and session
  a.close(ID,SID)
