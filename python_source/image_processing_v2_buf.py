## A collection of functions to analize the information
## contained in the images.
#cython: language_level = 3
import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image 
from natsort import natsorted
from tqdm import tqdm
from collections import OrderedDict as od
from bisect import bisect_left
import cv2
from collections import Counter 
from copy import deepcopy
from joblib import Parallel,delayed,Memory 
import pickle
from numba import njit
import compiled_functions as cf


def openImage(name,bw = 0) :
  return cv2.imread(name,bw)

@njit(nogil = True)
def normalize(imarray) :
  """This function takes a bidimensional ndarray as input and 
  normalizes it by dividing it by its spatial average.""" 
  minimum = np.min(imarray)
  maximum = np.max(imarray)
  return (imarray/np.average(imarray) - minimum)/(maximum - minimum)*255

@njit
def cutImage(imarray) :
  """This function takes a 576x768 array and cuts a little square
  inside the image."""
  return imarray[159:415,255:511]

def insideCutLoop(pathw,h) : 
  hs = str(h)
  ima = openImage(hs,0)
  cv2.imwrite((pathw+hs),cutImage(ima))
  

def cutImagesA(path,nArray) :
  """cuts all the images contained in path and stores them in a folder
  named cuts in the same folder"""
  os.mkdir(path+'/cuts')
  os.chdir(path)
  it = np.nditer(nArray)
  pathw = path+'/cuts/'
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideCutLoop)(pathw,h) for h in tqdm(it) )

def namesList(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and (entry.name.endswith('bmp') or entry.name.endswith('bmp')) :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return n_list

def namesArray(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and entry.name.endswith('bmp') :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return np.asarray(n_list)

def namesDict(path) :
  """This function scans the folder given by path and returns
  a dictionary containing the pairs (key : time , value : picture)."""
  
  with os.scandir(path) as fol :
    n_dict = { float(entry.name.split('_')[1]) : entry.name  for entry in fol if (entry.is_file() and (entry.name.endswith('bmp') or entry.name.endswith('bmp'))) }
    n_dict_sorted = od(sorted(n_dict.items(),key = lambda x : x[0]))
  
  return n_dict_sorted

def timesDiffs(ndict) :
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  return diffs

def findMinMaxTau(ndict) :#da sostituire magari con un tauDistr che trova la distribuzione dei tau e trova il picco, per stabilire come binnare i tau
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  print(diffs.index(max(diffs)))
  return max(diffs),round(float(keys[-1]),ndigits=2)

def fft(imagearray) :
  # compute the fft
  fft1 = np.fft.fft2(imagearray)
  # move the origin to the center of the image
  fft1 = np.fft.fftshift(fft1)
  # reduce the dynamic range of the magnitude (absolute value)
  fft1 = np.log10(np.abs(fft1))
  # compute the min and max value for normalization
  ma = np.nanmax(fft1[np.isfinite(fft1)])
  mi = np.nanmin(fft1[np.isfinite(fft1)])
  #print(mi,ma)
  # normalize
  fft1 = (fft1-mi)/(ma-mi)*255
  #print(fft1.shape)
  return (np.rint(fft1)).astype(np.int8)

def iFft(filename) :
  im = openImage(filename,cv2.IMREAD_GRAYSCALE)
  ffti = Image.fromarray(im,mode = 'L')
  return ffti

def insideFft(entry,path) :
  f = iFft(entry)
  f.save('fft/'+entry)

def folderFft(path,nArray) :
  """This function computes the fourier transform of the images
  contained in path. It then saves them in a folder called fft/"""
  os.chdir(path+'/cuts')
  os.mkdir('fft')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFft)(entry,path)for entry in tqdm(nArray))

def azimuthalAverage(immarray,center = None) :
  """This function takes an image in the form of a numpy ndarray and 
  computes the azimuthal average of the intensity of the pixels."""
  #attenzione, qui x e y sono scambiate, ma tanto non cambia molto
  shape = immarray.shape
 # print(shape)
  # find center inxes
  if center == None :
    xc,yc = (np.asarray(shape)/2).astype(int)-1
  else : 
    xc,yc = center
#  print(xc,yc)
  # set the maximum radius
  max_r = max(xc,yc)

  #determine the radius of each pixel of the image
  x, y = np.indices(shape)
  r = np.sqrt((x - xc)**2 + (y - yc)**2)
  
#  rad_couples = [(r[i,j],immarray[i,j]) for i in x for j in y]
  r = r.astype(np.int)
 # print(r)
  tbin = np.bincount(r.ravel(), immarray.ravel())
 # print(tbin)
  nr = np.bincount(r.ravel())
 # print(nr)
  azimuthalprofile = tbin / nr
  return azimuthalprofile   

y,x = np.indices((256,256))
r = np.sqrt((x-127)**2+(y-127)**2)
r = r.astype(np.int)
ra = r.ravel()
nr = np.bincount(ra)

def azimuthalAverageFast(immarray) :
  """This function basically do the same as the other azimuthal average,
  but here the r matrix is passed as an argument, so that can be computed 
  only once"""
  tbin = np.bincount(ra,immarray.ravel())
  return tbin/nr
###################################################################



###################################################################
def inside2ndLoop(im1,ks,k1,bins,results) :
  k1s = str(k1[1])
  #print('type of im: ',type(im), ' tipe of im1: ',type(im1), ' key: ',key,' key1: ',key1)
  tau = np.digitize(float(k1s.split(sep='_')[1]) - float(ks.split(sep='_')[1]),bins) - 1
  dif  =  k1[0]- im1
  results[tau] = cf.azimuthalAverageFast(abs(dif)**2)

def inside1stLoop(path,ndict,key,ndict1,results,bins) :
    im1 = openImage(path+'/'+ndict[key],cv2.IMREAD_GRAYSCALE)
    del ndict1[key]# controllare che in questo modo le coppie vengano fatte una volta sola
    # for each images in the data compute the difference of the ffts of the next
    # images and it
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(inside2ndLoop)(path,ndict1,im1,key,key1,bins,results) for key1 in tqdm(ndict1))

def bigLoop(path,nArray,nArray1,results,bins,imbuffer) :
  os.chdir(path+'/cuts/fft')
  it = np.nditer(nArray) 
  for k in tqdm(it) :
    ks = str(k)
    im1 = cf.oi(ks)
    nArray1 = np.delete(nArray1,0)# controllare che in questo modo le coppie vengano fatte una volta sola
      # for each images in the data compute the difference of the ffts of the next
      # images and it
    it1 = np.nditer(imbuffer)
    if nArray1.size > 0 :
      it2 = np.nditer(nArray1)
      Parallel(n_jobs = 4,prefer = 'threads')(delayed(inside2ndLoop)(im1,ks,k1,bins,results) for k1 in tqdm(zip(it1,it2)))

def insideFillBuffer(b,nArray,imbuffer) :
  imbuffer[b] = cf.oi(nArray[b])

def fillBuffer(path,nArray) :
  imbuffer = np.zeros((15000,576,768))
  size = min(nArray.size,15000)
  os.chdir(path)
  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBuffer)(b,nArray,imbuffer) for b in tqdm(range(size)))
  return imbuffer

####################################################################
########################### MAIN ###################################
###################################################################

if __name__ == "__main__" :

  path = input('Insert the path of the folder containing\nthe images to be processed:')
  nArray = namesArray(path)
  nArray1 = deepcopy(nArray)
  ndict = namesDict(path)
  print('Making the inventory:')
  print('  - number of images: ',len(nArray))
  total_time = float(nArray[-1].split(sep='_')[1])
  print('  - total duration of the measure (s): ',total_time)
  timeDiffs = Counter(timesDiffs(ndict))
  most_freq_diff = list(od(sorted(timeDiffs.items(),key = lambda x : x[1],reverse = True)).keys())[0]
  print('  - most frequent time difference (s): ',most_freq_diff)
  tau_bin_width = round(most_freq_diff,ndigits = 4)
  print('  - tau bin width: ',tau_bin_width)
  bin_number = int(total_time//tau_bin_width)+1
  bins =[0]+[tau_bin_width*k for k in range(bin_number-1)]
  print('bins ', len(bins))
  print(' - bin number: ',bin_number)
  q_number = 182 # controllare che sia effettivamente così
  results = np.zeros((bin_number,q_number))
 
  flag = input('Insert what type of analysis you want. Accepted answers:\n - all\n - wcuts\n - wfft\n')
  

  if flag == 'all' :
    print('selected: all')
    print('Ready to start')
    # cut the images
    print('Cutting the images...')
    cutImagesA(path,nArray)

    # compute ffts of the images in the folder and save them in the folder: fft.
    print('Computing the fft of the cut images. It may take a while...')
    folderFft(path,nArray)
    # here begins the main double loop over the images
    print('Now the big loop')
    imbuffer = fillBuffer(path,nArray)
    bigLoop(path,nArray,nArray1,results,bins,imbuffer)

  elif flag == 'wcuts' :
    print('selected: wcuts')
    print('Ready to start')

    # compute ffts of the images in the folder and save them in the folder: fft.
    print('Computing the fft of the cut images. It may take a while...')
    folderFft(path)
   
    # here begins the main double loop over the images
    print('Now the big loop')
    imbuffer = fillBuffer(path,nArray)
    bigLoop(path,nArray,nArray1,results,bins,imbuffer)

  elif flag == 'wfft' :
    print('selected: wfft')

    # here begins the main double loop over the images
    print('Now the big loop')
    imbuffer = fillBuffer(path,nArray)
    bigLoop(path,nArray,nArray1,results,bins,imbuffer)


  with open(path+'results.txt','w') as fi :
      np.savetxt(fi,results)
      # domanda, ma la fft come la faccio io va bene?
      # non solo: come tenere conto delle medie?
