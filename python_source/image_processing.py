## A collection of functions to analize the information
## contained in the images.

import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image 
from PIL import ImageStat as st
from natsort import natsorted
from tqdm import tqdm
import visual_utils as v
from collections import OrderedDict as od
from bisect import bisect_left
import cv2
from collections import Counter 

def cutImage(imarray) :
  """This function takes a 576x768 array and cuts a little square
  inside the image."""
  return imarray[159:415,255:511]

def cutImages(path,ndict) :
  """cuts all the images contained in path and stores them in a folder
  named cuts in the same folder"""
  os.mkdir(path+'cuts')
  print("cutting the images...\n")
  for k in tqdm(ndict) : 
    ima = cv2.imread(path+ndict[k],cv2.IMREAD_GRAYSCALE)
    ima = cutImage(ima)
    ima = Image.fromarray(ima)
    ima.save(path+'/cuts/'+ndict[k])
  

def take_closest(myList, myNumber):
  """
  Assumes myList is sorted. Returns closest value to myNumber.

  If two numbers are equally close, return the smallest number.
  """
  pos = bisect_left(myList, myNumber)
  if pos == 0:
    return myList[0]
  if pos == len(myList):
    return myList[-1]
  before = myList[pos - 1]
  after = myList[pos]
  if after - myNumber < myNumber - before:
   return after
  else:
   return before

def normalize(image) :
  """This function takes a bidimensional ndarray as input and 
  normalizes it in the range [0,255], so that can be considered 
  an image."""
  maxi = max(image.flat)
  return 255*(image-maxi)/(maxi-min(image.flat))

def namesList(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and (entry.name.endswith('tif') or entry.name.endswith('tiff')) :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return n_list

def namesDict(path) :
  """This function scans the folder given by path and returns
  a dictionary containing the pairs (key : time , value : picture)."""
  
  with os.scandir(path) as fol :
    n_dict = { float(entry.name.split('_')[1]) : entry.name  for entry in fol if (entry.is_file() and (entry.name.endswith('tif') or entry.name.endswith('tiff'))) }
    n_dict_sorted = od(sorted(n_dict.items(),key = lambda x : x[0]))
  
  return n_dict_sorted

def background(nList,extremes) :
  """This function return the backgroud image, as a ndarray, 
  givent the list of filenames and a tuple containing the
  indexes of the first and last image to use for background 
  calculation."""
  back = np.zeros((576,768))
  l = extremes[1] - extremes[0]
  for i in tqdm(range(extremes[0],extremes[1]+1)) :
    image = cv2.imread(nList[i],cv2.IMREAD_GRAYSCALE)
    back = back + image/l
  return np.rint(back).astype(np.uint8)

def backgroundPrecise(path,nList,extremes) :
  """This function return the backgroud image, as a ndarray, 
  givent the list of filenames and a tuple containing the
  indexes of the first and last image to use for background 
  calculation. This version of the function doesn't cast the 
  result into 8-bit uint image, but returns a float matrix."""
  back = np.zeros((256,256))
  l = extremes[1] - extremes[0]
  for i in tqdm(range(extremes[0],extremes[1]+1)) :
    image = cv2.imread(path+'/'+nList[i],cv2.IMREAD_GRAYSCALE)
    back = back + image/l
  return back

def clean(path,extremes) :
  """This function cleans the images found in path subtracting
  the background calculated with the images in extremes."""
  offset = 0
  print('Reading the file list...')
  nlist = namesList(path)
  print('Done.\nCalculating the background...')
  back = background(path,nlist,extremes)
  os.mkdir('cleaned')
  print('Done.\nSubtracting the background and saving the new images in cleaned/...')
  for i in tqdm(nlist) :
    im = cv2.imread(i)
    cl = im.astype('int16') - back.astype('int16')
    Im = Image.fromarray(np.rint((cl+120)*0.75).astype('uint8'))
    Im.save('cleaned/'+i)

def cleanPrecise(path,extremes) :
  """This function cleans the images found in path subtracting
  the background calculated with the images in extremes."""
  offset = 0
  print('Reading the file list...')
  nlist = namesList(path)
  print('Done.\nCalculating the background...')
  back = backgroundPrecise(path,nlist,extremes)
  os.mkdir(path+'/'+'cleaned')
  print('Done.\nSubtracting the background and saving the new images in cleaned/...')
  for i in tqdm(range(len(nlist))) :
    im = cv2.imread(path+'/'+nlist[i])
    cl = im.astype('int16') - back
    Im = Image.fromarray((cl+120)*0.75, mode = 'F')
    Im.save(path+'/cleaned/'+nlist[i])

def cleanArray(array,background) :
  cl = array.astype('int16') - background
  return (cl+120)*0.75

def findMinMaxTau(ndict) :
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  print(diffs.index(max(diffs)))
  return max(diffs),round(float(keys[-1]),ndigits=2)

def timesDiffs(ndict) :
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  return diffs

def findMaxTau(ndict) :
  keys = [l for l in ndict.keys()]
  return keys[-1]
## statistical analysis ##

def var(path,plot = False) : 
  """This function computes the variance of the images contained
  in path and stores them in a file called var.txt. If plot is 
  True, it also displays a plot of the variance vs time. """

  with open(path+'var.txt','w') as f, os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and (entry.name.endswith('tif') or entry.name.endswith('tiff')) :
        im = Image.open(path+entry.name)
        f.write(entry.name.split('_')[1]+'  '+str(st.Stat(im).var[0])+'\n')
  
  if plot :
   v.plotVar(path+'var.txt') 

def fft(imagearray) :
  # compute the fft
  fft1 = np.fft.fft2(imagearray)
  # move the origin to the center of the image
  fft1 = np.fft.fftshift(fft1)
  # reduce the dynamic range of the magnitude (absolute value)
  fft1 = np.log10(np.abs(fft1))
  # compute the min and max value for normalization
  ma = np.nanmax(fft1[np.isfinite(fft1)])
  mi = np.nanmin(fft1[np.isfinite(fft1)])
  #print(mi,ma)
  # normalize
  fft1 = (fft1-mi)/(ma-mi)*255
  #print(fft1.shape)
  return fft1

def iFft(filename) :
  im = cv2.imread(filename)
  fft2 = fft(im)
  ffti = Image.fromarray(fft2)
  return ffti


def fftAll(path) :
  """This function computes the fourier transform of the images
  contained in path. It then saves them in a folder called fft/"""

  with os.scandir(path) as fol :
    os.mkdir('fft')
    for entry in fol :
      if entry.is_file() and entry.name.endswith('tif') or entry.name.endswith('tiff') :
        f = i_fft(entry.name)
        f.save('fft/'+entry.name)

def diff(path,ndict,t,tau,background) : 
  """This function takes the path of the folder containing the pictures,
  the dictionary containing the names of the pictures, the time at wich
  one wants to calculate the difference, and the delay tau, and returns 
  a tuple containing the real time, the real tau and the image difference 
  as a numpy ndarray."""
  
  # find the time, among those measured, closest to t
  real_t = take_closest(list(ndict.keys()),t)
  # find the time, among those measured, closest to t+tau
  real_tau = take_closest(list(ndict.keys()),t+tau)
  if real_t == real_tau : 
    print('WAIT : hai scelto un tau troppo piccolo')
  # open the image corresponding to time t
  im_t = cv2.imread(path+ndict[real_t],cv2.IMREAD_GRAYSCALE) 
  # open the image corresponding to time t+tau
  im_tau = cv2.imread(path+ndict[real_tau],cv2.IMREAD_GRAYSCALE)
  
  return (real_t,real_tau,cleanArray(im_t,background) - cleanArray(im_tau,background))

def fftDiff(path,ndict,t,tau) :
  """This function takes the path of the folder containing the pictures,
  the dictionary containing the names of the pictures, the time at wich
  one wants to calculate the difference, and the delay tau, and returns 
  a tuple containing the real time, the real tau and the fft of the 
  difference as a numpy ndarray."""
  
  (rt,rta,df) = diff(path,ndict,t,tau)
  return (rt,rta,fft(df))

def radialAverage(immarray,center = None) :
  """This function takes an image in the form of a numpy ndarray and 
  computes the radial average of the intensity of the pixels."""
  #attenzione, qui x e y sono scambiate, ma tanto non cambia molto
  shape = immarray.shape
 # print(shape)
  # find center inxes
  if center == None :
    xc,yc = (np.asarray(shape)/2).astype(int)-1
  else : 
    xc,yc = center
#  print(xc,yc)
  # set the maximum radius
  max_r = max(xc,yc)

  #determine the radius of each pixel of the image
  x, y = np.indices(shape)
  r = np.sqrt((x - xc)**2 + (y - yc)**2)
  
#  rad_couples = [(r[i,j],immarray[i,j]) for i in x for j in y]
  r = r.astype(np.int)
 # print(r)
  tbin = np.bincount(r.ravel(), immarray.ravel())
 # print(tbin)
  nr = np.bincount(r.ravel())
 # print(nr)
  radialprofile = tbin / nr
  return radialprofile   

y,x = np.indices((256,256))
r = np.sqrt((x-127)**2+(y-127)**2)
r = r.astype(np.int)
nr = np.bincount(r.ravel())

def radialAverageFast(immarray) :
  """This function basically do the same as the other radial average,
  but here the r matrix is passed as an argument, so that can be computed 
  only once"""
  tbin = np.bincount(r.ravel(),immarray.ravel())
  return tbin/nr


def radialAverage1(immarray,b,center = None) :
  """This function takes an image in the form of a numpy ndarray and 
  computes the radial average of the intensity of the pixels."""

  shape = immarray.shape
  #print(shape)
  # find center inxes
  if center == None :
    xc,yc = (np.asarray(shape)/2).astype(int)-1
  else : 
    xc,yc = center
  #print(xc,yc)
  # set the maximum radius
  max_r = max(xc,yc)

  #determine the radius of each pixel of the image
  x, y = np.indices(shape)
  r = np.sqrt((x - xc)**2 + (y - yc)**2)
#  rad_couples = [(r[i,j],immarray[i,j]) for i in x for j in y]
 # r = r.astype(np.int)
  tbin = np.histogram(r.ravel(), weights = immarray.ravel(),range = (0,max_r),bins = b)
#  print(tbin,'\n\n\n')
  nr = np.histogram(r.ravel(),range = (0,max_r),bins = b)
#  print('\n',nr[0])
  return tbin[0]/(2*np.pi*np.arange(1,max_r+1,max_r/b)),tbin[1]



#def averageDiff(path, ndict, tau, n_av, t_range,background) : 
#  """This function takes the path, the ndict and a value of the delay tau as
#  input and computes the average (on n_av values) of the differences of 
#  images. t_range is the tuble with the minimum and maximum values of time 
#  to consider for the analysis."""
#
# av_array = np.zeros((576,768,n_av))
# len_range = t_range[1]-t_range[0]
# step = int(np.rint(len_range/n_av))
# steps = [t_range[0]+n*step for n in range(n_av)]
# for j in range(n_av) :
#   av_array[:,:,j] = diff(path,ndict,steps[j],tau,background)[2]/n_av
# return av_array.sum(axis = 2)

def tauPoint(path,ndict,tau,n_av,t_range,background) :
  """This function takes the path, the ndict as input
  and computes the value of the structure function for a single value of tau
  i.e. it makes the squared fft of the average difference of the images spaced
  by tau. The result is an image. To obtain a result for a single q, a radial
  average has to be taken."""
  
  average_tau_point = np.zeros((182,))
  len_range = t_range[1]-t_range[0]
  step = int(np.rint(len_range/n_av))
  steps = [t_range[0]+n*step for n in range(n_av)]
  for k in tqdm(range(n_av)) : 
    average_tau_point = average_tau_point + radialAverageFast(abs(fft(diff(path,ndict,steps[k],tau,background)[2]))**2/n_av)
  return average_tau_point

def availDiffs(ndict) :
  array = np.asarray([k[0]for k in ndict.items()],dtype = np.float32)
  array = np.reshape(array,(len(array),1))
  print(array.shape)
  diff_matrix = array-array.transpose()
  ixs = np.where(diff_matrix > 0)
  print(diff_matrix.shape)
  return Counter(diff_matrix[ixs[0],ixs[1]].flat)

#def tauPointDiffs(ndict,maxtau,mintau) :
#  for j in ndict :
     
#  average_tau_point = np.zeros((481,))
#  for k in tqdm(range(n_av)) : 
#    average_tau_point = average_tau_point + radialAverageFast(abs(fft(diff(path,ndict,steps[k],tau,background)[2]))**2/n_av)
#  return average_tau_point

def structureFunction(path,ndict,n_av,t_range,q,min_tau,max_tau, tau_step,background) :
  """This functin takes the usual args plus the wavenumber one wants to
  calculate the structure function at, and the maximum delay desired and 
  returns an array containing the pirs to build the plot. tau_steps is  
  time resolution (in seconds) desired for the tau points"""
  
  # number of tau points at which evaluate the function
  n_points = int(np.floor((max_tau-min_tau)/tau_step))
  # list containing the tau points
  tau_points = [min_tau+k*tau_step for k in range(1,n_points+1)]
  print(tau_points)
  # initialize the output array
  plot = np.zeros((n_points,2))

  for j in tqdm(range(n_points)) :
    plot[j] = [tau_points[j],tauPoint(path,ndict,tau_points[j],n_av,t_range,background)[q]]

  return plot
