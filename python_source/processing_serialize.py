## A collection of functions to analize the information
## contained in the images.
#cython: language_level = 3
import os
import gc
import numpy as np
import matplotlib.pyplot as plt
from natsort import natsorted
from tqdm import tqdm
from collections import OrderedDict as od
from bisect import bisect_left
import cv2
from collections import Counter 
from copy import deepcopy
from joblib import Parallel,delayed,Memory 
import pickle
from numba import njit
import json
from skimage.feature import register_translation as rt
from skimage.filters import gaussian as gs
from scipy.optimize import curve_fit as fit
import pandas as pd

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
#####################################################################
################ I/O and basic preprocessing #######################

def take_closest(myArray, myNumber):
  """Assumes myList is sorted. Returns closest value to myNumber.
  If two numbers are equally close, return the smallest number.
  """
  myList = list(myArray)
  pos = bisect_left(myList, myNumber)
  if pos == 0:
    return myList[0]
  if pos == len(myList):
    return myList[-1]
  before = myList[pos - 1]
  after = myList[pos]
  if after - myNumber < myNumber - before:
   return after
  else:
   return before
## functions to open the images
def openImage(name,bw = 0) :
  return (cv2.imread(name,bw)).astype(np.float64)

def openImage8(name,bw = 0) :
  return cv2.imread(name,bw)

def normalize(imarray) :
  """This function takes a bidimensional ndarray as input and 
  normalizes it by dividing it by its spatial average.""" 
  return imarray/np.average(imarray)
# to open multiple image to cut them (maybe unused)
def insideFillBufferCu(b,ndict,imbuffer) : 
  imbuffer[b] = openImage8(ndict[b])

def fillBufferCu(path,ndict,timesArray,imbuffer,image_step) :
  """This function fill an ordered dict with the subsect of the imagese to 
  analyse. So it needs the image step and the time extremes to consider."""

  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBufferCu)(b,ndict,imbuffer) for b in tqdm(timesArray))
  return imbuffer


#optimized function to cut images in the center
@njit
def cutImage(imarray) : # domanda: come fai a essere sicuro che l'immagine è centrata? è importante? aggiungere nu argomento: posizione del centro?
  """This function takes a 576x768 array and cuts a little square
  inside the image."""
  return imarray[159:415,255:511]

# cut routine with buffers
def insideCutLoop(pathw,ndict,imbuffer,h) : 
  cv2.imwrite((pathw+ndict[h]),cutImage(imbuffer[h]))
  

def cutImagesA(path,ndict,timesArray) :
  """cuts all the images contained in path and stores them in a folder
  named cuts in the same folder"""
  os.mkdir(path+'/cuts')
  os.chdir(path)
  pathw = path+'/cuts/'
  imbuffer = dict()
  for step in tqdm(range(len(nArray)//1000)) :
    times_ar = timesArray[step*1000:step*1000+1000]
    imbuffer = fillBufferCu(path,ndict,times_ar,imbuffer,image_step = 1)
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideCutLoop)(pathw,ndict,imbuffer,h) for h in tqdm(times_ar) )
  remains = timesArray[step*1000+1000:]
  imbuffer = fillBufferCu(path,ndict,remains,imbuffer,image_step = 1)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideCutLoop)(pathw,ndict,imbuffer,h) for h in tqdm(imbuffer) )

# sequential cut routine
def cutImages(path,ndict,timesArray,beginning,end) :
  """cut images sequentially, without buffer of any sort"""
  pathw = path+'/cuts/'
  pathd = path+'/'
  if not os.path.exists(pathw) :
    os.mkdir(pathw)
  os.chdir(pathw)
  for t in tqdm(timesArray[max(0,beginning-1):end]) :
    cv2.imwrite(ndict[t],cutImage(openImage(pathd+ndict[t])))

# some functions to read into the folders and collect the names of the files
def namesList(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and (entry.name.endswith('bmp') or entry.name.endswith('tif')) :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return n_list

def namesArray(path) :
  """This function scans the folder given by path and returns
  the ordered list of filenames."""
  n_list = []
  with os.scandir(path) as fol :
    for entry in fol :
      if entry.is_file() and entry.name.endswith('bmp') :
        n_list.append(entry.name)
  n_list = natsorted(n_list)
  return np.asarray(n_list)

def namesDict(path) :
  """This function scans the folder given by path and returns
  a dictionary containing the pairs (key : time , value : picture)."""
  
  with os.scandir(path) as fol :
    n_dict = { float(entry.name.split('_')[1]) : entry.name  for entry in fol if (entry.is_file() and (entry.name.endswith('bmp') or entry.name.endswith('tif'))) }
    n_dict_sorted = dict(sorted(n_dict.items(),key = lambda x : x[0]))
  
  return n_dict_sorted

def timesArray(ndict) :
  return np.array([x[0] for x in ndict.items()])

#very useful function to take a selection of images, by their times, for the
# analysis
def pickTimes(timesArray,time_extremes,image_step) :
  sx_index = np.where(timesArray == time_extremes[0])[0][0]
  dx_index = np.where(timesArray == time_extremes[1])[0][0]
  return timesArray[sx_index:dx_index:image_step]

#parallelized routines to load a number of images in the memory
def insideFillBuffer(b,ndict,imbuffer) : 
  imbuffer[b] = normalize(openImage(ndict[b]))

def fillBuffer(path,ndict,timesArray,imbuffer) :
  """This function fill an ordered dict with the subsect of the imagese to 
  analyse. So it needs the image step and the time extremes to consider."""

  os.chdir(path+'/cuts')

  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBuffer)(b,ndict,imbuffer) for b in tqdm(timesArray))
  gc.collect()
  return imbuffer

# the same but for the translation function
def insideFillBufferTr(b,ndict,imbuffer) : 
  imbuffer[b] = (openImage(ndict[b]))[223:351,319:447]

def fillBufferTr(path,ndict,timesArray,imbuffer,image_step) :
  os.chdir(path)
  print('Filling the image buffer. Please wait...')
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBufferTr)(b,ndict,imbuffer) for b in tqdm(timesArray[::image_step]))
  return imbuffer

## for the gaussian blur filter
def insideFillBufferGs(b,ndict,imbuffer) : 
  imbuffer[b] = (openImage(ndict[b]))

def fillBufferGs(path,ndict,timesArray,imbuffer,image_step) :
  os.chdir(path)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFillBufferGs)(b,ndict,imbuffer) for b in tqdm(timesArray[::image_step]))
  return imbuffer
######
# function that computes all the consecutive time diffs (maybe could be
# modified to work with timesArray, more efficiently
def timesDiffs(ndict) :
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  return diffs

# to find the maximum delay available in a dataset (unused)
def findMinMaxTau(ndict) :#da sostituire magari con un tauDistr che trova la distribuzione dei tau e trova il picco, per stabilire come binnare i tau
  keys = [l for l in ndict.keys()]
  diffs = [round(round(float(keys[k+1]),ndigits = 2)-round(float(keys[k]),ndigits =2),ndigits = 2) for k in range(len(keys)-1)]
  print(diffs.index(max(diffs)))
  return max(diffs),round(float(keys[-1]),ndigits=2)

def binArrayCenters(bins) :
  return np.asarray([(bins[ie]+bins[ie+1])/2 for ie in range(len(bins)-1)])

#####################################################################
################## image manipulation ###############################

def fft(imagearray) : # capire con il prof se questa cosa ha senso
  # compute the fft
  fft1 = np.fft.fft2(imagearray)
  # move the origin to the center of the image
  fft1 = np.fft.fftshift(fft1)
  # reduce the dynamic range of the magnitude (absolute value)
  #fft1 = np.log10(np.abs(fft1))
  # compute the min and max value for normalization
 # ma = np.nanmax(fft1[np.isfinite(fft1)])
 # mi = np.nanmin(fft1[np.isfinite(fft1)])
  #print(mi,ma)
  # normalize
 # fft1 = (fft1-mi)/(ma-mi)*255
  #print(fft1.shape)
  return fft1

def insideFft(ini,imbuffer) :
  imbuffer[ini] = fft(imbuffer[ini])


def folderFft(imbuffer) :
  """This function computes the fourier transform of the images
  contained in path. It then saves them in a folder called fft/"""
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideFft)(ini,imbuffer) for ini in tqdm(imbuffer))


y,x = np.indices((256,256))
r = np.sqrt((x-127)**2+(y-127)**2)
r = r.astype(np.int)
ra = r.ravel()
nr = np.bincount(ra)

def azimuthalAverageFast(immarray) :
  """This function basically do the same as the other azimuthal average,
  but here the r matrix is passed as an argument, so that can be computed 
  only once"""
  tbin = np.bincount(ra,immarray.ravel())
  return tbin/nr

# routines to apply gaussian blur to images
def insideGaussian(ndict,pathw,imbuffer,correction,key) :
  cv2.imwrite(ndict[key],gs(imbuffer[key],sigma = 5)) 

def GaussianBlur(path,ndict,timesArray,time_stop,image_step = 10) :# aggiungere anche un timestep per velocizzare? magari non è necessario proprio farle tutte 
  """This function takes the images in the folder and applies a gaussian filter to get 
  rid of the faster noise."""
  correction = dict()
  os.mkdir(path+'/gaussian_filtered')
  pathw = path+'/gaussian_filtered'
  imbuffer = dict()
  times_ar = timesArray[::image_step]
  times_ar = times_ar[times_ar<time_stop]
  for step in tqdm(range(max(1,len(times_ar)//1000))) :
    imbuffer = fillBufferGs(path,ndict,times_ar[step*1000:step*1000+1000],imbuffer,image_step = 1)
    os.chdir(pathw)
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideGaussian)(ndict,pathw,imbuffer,correction,key) for key in tqdm(imbuffer)) 
  remains = times_ar[step*1000+1000:]
  imbuffer = fillBufferGs(path,ndict,remains,imbuffer,image_step = 1)
  os.chdir(pathw)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideGaussian)(ndict,pathw,imbuffer,correction,key) for key in tqdm(imbuffer)) 

#####################################################################
################ Dynamic analysis ###################################

def bigLoop(results,bins,imbuffer,timesArray,average_array,max_tau) :
  timesArray1 = deepcopy(timesArray)
  co = 0
  for k in tqdm(timesArray) :
    co = co+1
    im = imbuffer[k]
      # for each images in the data compute the difference of the ffts of the next
      # images and it
    if len(ndict) > 0 :
      for k1 in timesArray1[co:] :
        if k1 - k > max_tau :
          break
        else :
          tau = np.searchsorted(bins,k1 - k,side = 'left') - 1
          dif  = imbuffer[k1]- im
          results[tau] = results[tau] + azimuthalAverageFast(np.abs(dif)**2)
          average_array[tau] = average_array[tau] + 1
  return ('Last time analysed is: '+str(k))

###################################################################
########################## static analysis ########################
def opticalBackgroud(path,ndict,timesArray,start_time,end_time) :
  """computes the optical background as average of images"""
  start = take_closest(timesArray,start_time)
  end = take_closest(timesArray,end_time)
  times = pickTimes(timesArray,(start,end),1)
  imbuffer = dict()
  imbuffer = fillBuffer(path,ndict,times,imbuffer)
  # NB here the images are already normalized
  back = np.zeros((256,256))
  for im in imbuffer :
    back = back + imbuffer[im]
  np.save('back.npy',back/len(imbuffer))
  return back/len(imbuffer)


def staticSpectrum(imarray, optical_back) : 
  """It returns the square modulus of the azimuthal average of the
  fft of the signal image, i.e. the difference between imarray and
  the optical background"""
  return azimuthalAverageFast((np.abs(fft(imarray-optical_back)))**2)


def staticSpectraM(path,ndict,timesArray,start_time, end_time,opt_back) :
  """returns the measured static spectra (not yet averaged) where the 
  electronic background B(q) has not yet been subtracted"""

  os.chdir(path+'/cuts')
  start = take_closest(timesArray,start_time)
  end = take_closest(timesArray,end_time)
  times = pickTimes(timesArray,(start,end),1)
  spectra = pd.DataFrame(np.zeros((len(times),182)), index = times, columns = range(1,183))
  for t in tqdm(times) :
      spectra.loc[t,:] = staticSpectrum(normalize(openImage(ndict[t])),opt_back)
  return spectra

def backgroundSpectrum(path,ndict,timesArray,start_time,end_time) :
  
  start = take_closest(timesArray,start_time)
  end = take_closest(timesArray,end_time)
  times = pickTimes(timesArray,(start,end),1)
##################################################################
########################## statistical routines #####################
def averageImage(ndict,seconds) :
  avi = np.zeros((256,256))
  counter = 0
  for key in ndict :
    if key > seconds :
      break
    counter = counter +1
    avi = avi + openImage(ndict[key])
  print(counter,key)
  return avi/counter, counter

def insideVar(ai,key,ndict,vdict) :
  vdict[key] = np.sum((openImage(ndict[key])-ai)**2)

def folderVar(ndict,ai,number_of_images) :
  vdict = dict()
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideVar)(ai,key,ndict,vdict) for key in tqdm(ndict))
  return vdict

def movingAverage(array, n=3) :
    ret = np.cumsum(array, dtype=np.float64)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

#####################################################################
################# charaterizing the traslation ######################
def insideTranslation(imbuffer,correction,key,im1,upsampling) :
  correction[key] = rt(imbuffer[key],im1,upsample_factor = upsampling)[0]

def translationCorrection(path,ndict,timesArray,time_stop,image_step = 10,upsampling = 1) :# aggiungere anche un timestep per velocizzare? magari non è necessario proprio farle tutte 
  """This function takes the images in the folder and computes the 
  time crosscorrelation function with respect to the first image, in 
  order to see how the whole image tralsates in the plane during the 
  transient."""
  correction = dict()
  os.chdir(path)
  imbuffer = dict()
  im1 = openImage(ndict[timesArray[0]])[223:351,319:447]
  times_ar = timesArray[::image_step]
  times_ar = times_ar[times_ar<time_stop]
  for step in tqdm(range(max(1,len(times_ar)//1000))) :
    imbuffer = fillBufferTr(path,ndict,times_ar[step*1000:step*1000+1000],imbuffer,image_step = 1)
    Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideTranslation)(imbuffer,correction,key,im1,upsampling) for key in tqdm(imbuffer)) 
  remains = times_ar[step*1000+1000:]
  imbuffer = fillBufferTr(path,ndict,remains,imbuffer,image_step = 1)
  Parallel(n_jobs = 4,prefer = 'threads')(delayed(insideTranslation)(imbuffer,correction,key,im1,upsampling) for key in tqdm(imbuffer)) 

  with open(path+'/traslation'+str(upsampling)+'.json','w') as fil :
    json.dump(correction,fil,cls = NumpyEncoder)

#####################################################################
########################## Fit ######################################

def StructureFunction(taus,A,tau,B) :
  return A*(1-np.exp(-taus/tau)) + B

def Fit(filename,guess = None,extremes = (-np.inf,np.inf)) : 
  """filename must be the path of a file containing the output of the 
  dynamic analysis, formatted as csv. This function fits the data against 
  the Structure Function, for every q, starting from the initial guess of the parameters,
  if provided. it returns a pandas dataframe containing q_max rows, where every row contains the
  results of the fit for the corresponding q."""
  
  results = pd.read_csv(filename,index_col = 0) 
  taus = results.index
  fits = pd.DataFrame(np.zeros((results.shape[1],4)), index = results.columns, columns = ['A','Tau','B','R_squared'])
  guess = guess
  for i in range(1,results.shape[1]+1):
    print(i,flush = True)
    popt, pcov = fit(StructureFunction,taus,results[str(i)].to_numpy(),p0 = guess,bounds = extremes)
    print(popt)
    residuals = results[str(i)] - StructureFunction(taus,popt[0],popt[1],popt[2])
    ss_res = np.sum(residuals**2)
    ss_tot = np.sum((results[str(i)]-np.mean(results[str(i)]))**2)
    r_squared = 1 - (ss_res / ss_tot)
    fits.iloc[i-1] = [popt[0], popt[1], popt[2], r_squared]
    guess = [popt[0],popt[1],popt[2]]
  return fits


def FitNp(filename,q,guess = None,extremes = (-np.inf,np.inf)) : 
  """filename must be the path of a file containing the output of the 
  dynamic analysis, formatted as usual. This function fits the data against 
  the Structure Function, for a particular q, starting from the initial guess of the parameters,
  if provided."""
  
  results = np.loadtxt(filename) 
  taus = [x[0] for x in results]
  sf = results[:,q]
  fits = pd.DataFrame()
  popt, pcov = fit(StructureFunction,taus,sf,p0 = guess,bounds = extremes)
  return popt
#####################################################################
########################### MAIN ####################################
#####################################################################

if __name__ == "__main__" :

  path = input('Insert the path of the folder containing\nthe images to be processed:')
  nArray = namesArray(path)
  nArray1 = deepcopy(nArray)
  ndict = namesDict(path)
  timesArray = timesArray(ndict)
  print('Making the inventory:')
  print('  - number of images: ',len(nArray))
  total_time = float(nArray[-1].split(sep='_')[1])
  print('  - total duration of the measure (s): ',total_time)
  timeDiffs = Counter(timesDiffs(ndict))
  most_freq_diff = list(dict(sorted(timeDiffs.items(),key = lambda x : x[1],reverse = True)).keys())[0]
  print('  - most frequent time difference (s): ',most_freq_diff)
  flag = input('Insert what type of analysis you want. Accepted answers:\n - cut\n - static\n - dynamic\n - variance\n')
  

  if flag == 'cut' :
    print('selected: cut')
    # cut the images
    beginning = int(input('Insert the beginning image number: '))
    end = int(input('Insert the end image number: '))
    print('Cutting the images...')
    cutImages(path,ndict,timesArray,beginning,end)
    print('Done.')

  elif flag == 'dynamic' :
    print('selected: dynamic')
    width = float(input('If you want a coarser binning, insert a width greater than the most\nfrequent time difference: '))
    tau_bin_width = max(round(most_freq_diff,ndigits = 4),width)
    print('  - tau bin width: ',tau_bin_width)
    q_number = 182 # controllare che sia effettivamente così
    image_step = int(input('Insert the image_step, it must be an integer: '))
    time_inf = float(input('Insert the beginning time of the time window you want to consider: '))
    time_sup = float(input('Insert the ending time of the time window you want to consider: '))
    max_tau = float(input('Insert the maximum delay value you want to consider for the analysis: '))
    time_range = (take_closest(timesArray,time_inf),take_closest(timesArray,time_sup))
    print('time range selected: ['+str(time_range[0])+','+str(time_range[1])+']')
    print('filling the image buffer...')
    timesArray = pickTimes(timesArray,time_range,image_step)
    new_total_time = timesArray[-1] - timesArray[0]
    bin_number = int(max_tau//tau_bin_width)+1
    bins =[0]+[tau_bin_width*k for k in range(1,bin_number+1)]
    print('bins ', len(bins))
    print(' - bin number: ',bin_number)
    results = np.zeros((bin_number,q_number))
    average_array = np.zeros(bin_number) # to store the number of differences with that tau value
    imbuffer = dict({ky : np.zeros((256,256)) for ky in timesArray})
    imbuffer = fillBuffer(path,ndict,timesArray,imbuffer)
    
    # compute ffts of the images in the folder and save them in the folder: fft.
    print('Computing the fft of the cut images. It may take a while...')
    folderFft(imbuffer)
   
    # here begins the main double loop over the images
    print('Now the big loop')
    lastt = bigLoop(results,bins,imbuffer,timesArray,average_array,max_tau)
    print(lastt)
    print(min(results.ravel())) 
    for rowi in range(len(results)) :
      if average_array[rowi] != 0 :
        results[rowi] = results[rowi]/average_array[rowi]
    bincenters = binArrayCenters(bins)
#    results = np.hstack((bincenters,results))
    resultsDF = pd.DataFrame(results,index = bincenters,columns = range(1,183))

    resultsDF.to_csv(path+'/results_'+str(time_range[0])+'_'+str(time_range[1])+'_'+str(image_step)+'_'+str(tau_bin_width)+'.csv')
      # domanda, ma la fft come la faccio io va bene?
      # non solo: come tenere conto delle medie?
    
  elif flag == 'variance' :
    print('selected: variance')
    os.chdir(path+'/cuts')
    seconds = float(input('Insert the seconds of measure to consider for averaging: '))
    # compute the average image form the first 5 minutes
    ai,number  = averageImage(ndict,seconds)
    # compute the variance array
    number_of_images = len(nArray) - number 
    vdict = folderVar(ndict,ai,number_of_images)
    with open(path+'/variance.json','w') as fil :
      json.dump(vdict,fil)
      # fare media mobile e magari sottrarre il primo punto (ma non ho capito bene questa cosa)

  elif flag == 'static' :
    # calculate background spectrum
    start_time = float(input('Insert the start time for the optical background: '))
    end_time = float(input('Insert the end time for the optical background: '))
    back = opticalBackgroud(path,ndict,timesArray,start_time,end_time)
#    back = backgroundSpectrum(path,ndict,timesArray,start_time,end_time) 

    #now the spectra
    start_time = float(input('Insert the start time for the spectra: '))
    end_time = float(input('Insert the end time for the spectra: '))
    name = input('Insert "spectrum" or "background": ')

    spectra = staticSpectraM(path,ndict,timesArray,start_time, end_time, back)
    if name == 'spectrum' :
      spectra.to_csv(path+'/m_static_spectra_'+str(start_time)+'_'+str(end_time)+'.csv')
    else :
      spectra.to_csv(path+'/B_q_'+str(start_time)+'_'+str(end_time)+'.csv')
