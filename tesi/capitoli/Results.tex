\chapter{Results and discussion}
\label{chap:sigh}
  The goal of this study was to characterize the stability and the non-equilibrium concentration fluctuations of a colloidal 
  suspension made of water and LUDOX\reg \hspace{2pt}TMA nanoparticles in a thermodiffusion experiment. 
  
  In the first part of the work, we have surveyed previous work on the instability of the sample near the convection threshold, observing how the initial condition 
  and the history of the sample greatly affect its stability. In particular, in~\cref{sec:rotating} we will describe how, in the parameter domain where the sample 
  should be stable with respect to Rayleigh-Bénard and solutal convection, we observed the formation 
  of localized convective structures, that exhibit a peculiar rotating transient state, before disappearing. 

  In the second part of the chapter (see~\cref{sec:fluc}), we tested the suitability of this sample for the study of the onset and development of
  NEFs in a colloidal suspension.

  \section{Convective instabilities}
  \label{sec:rotating}
  As described in~\cref{sec:inst}, heat conduction in fluids is greatly affected by the presence of colloidal particles that exhibit a thermophilic
  behaviour. In particular, the presence of (even a small amount of) thermophilic particles, has a stabilizing effect when the suspension is heated
  from below and, conversely, it has a destabilizing effect when heating from above. As a consequence, convection can be driven by two different contributions to the 
  density profile: the  thermal contribution, when 
  convection  is driven by the difference in density in different portions of the fluid with different temperatures, or the solutal one, when, heating the sample from above, 
  the colloidal particles diffuse towards the upper, warmer, boundary, and are then pulled down by gravity (see~\cref{fig:phenom}, for a pictorial representation of the two 
  cases).

  LUDOX\reg TMA particles have a negative Soret coefficient (see~\cref{tab:ludox}), and therefore are thermophilic. In order to study 
  non-equilibrium fluctuations, there must not be convection in the sample. Thus, in the first part of the work,
  we explored the conditions under wich our sample is stable against convection. 

  First of all, we determined the convective threshold of our sample. This threshold can in principle be calculated, and it corresponds to the
  temperature difference, $\Delta T = T_\textup{above} - T_\textup{below}$, at which the Rayleigh number attains the critical value $Ra_\textup{c} = 1708$. 
  In our case, this corresponds
  to $\Delta T_\textup{theo} \approx \SI{-2.44}{\celsius}$. This result applies to a layer of sample with infinite aspect ratio, defined as the ratio between the radius 
  and the thickness of the sample. In our case the aspect ratio is  large, of the order of $\SI{13.5}{\milli\metre}/\SI{3.1}{\milli\metre}=4.4$, and 
  the approximation is expected to hold. 
  However, as already mentioned, the presence of the colloidal particles changes effectively 
  the value of the threshold. Indeed, we found that the temperature difference at which, with a cell of height $d = \SI{3.1}{\milli\metre}$ and a concentration
  of LUDOX\reg TMA of $4 \%$ w/w, convection starts inside the sample, it's approximatively $\Delta T_\textup{eff} \approx \SI{-4.5}{\celsius}$. 
  It is important to note that the values of the temperature differences that we reported above are calculated considering only the sample thickness. Actually,
  when doing the experiments, the temperatures that get controlled are the temperatures measured on the external surfaces of the sapphire plates.
  The temperature drop across the thickness of the plates depends on the magnitude of the applied gradient, the thermal properties of the measured cell, the heat
  conduction regime under which the experiment is done (i.e. if there is convection or not inside the sample) and the geometry of the sample cell. 
  In the conditions under which our experiments have been carried out, we were able to estimate that the temperature difference between the internal surfaces of the 
  sapphire plates is approximatively the $75 \%$ of the external $\Delta T$. 
  The temperature differences reported in the rest of the thesis  have to be intended as those on the internal surfaces of the plates,  obtained from the measured ones, 
  and converted according to this estimate.

  According to what has been said above, in principle, for the purpose of our study it is sufficient to stay below that threshold to avoid convection.~\footnote{Note that, when the concentration of the
  colloidal particles is changed, also the threshold changes.} However, the values of the temperature gradient
  and the sample concentration (along with the geometry of the cell) aren't the only factors affecting the stability of the suspension. Indeed, the way the sample is 
  prepared (that we will call its \emph{initial conditions}) before beginning the experiment, has a great influence on the stability~\cite{Donzelli2009}. In particular, the 
  vertical concentration profile of the colloidal particles when the temperature difference is imposed plays a crucial role. If the concentration of the particles is 
  stratified (due to sedimentation determined by gravity, the presence of a pressure gradient inside the sample, directed towards the bottom, causes  a stabilizing
  concentration gradient) the only condition to avoid convective instabilities is to stay below the temperature threshold. On the 
  contrary, when the concentration of particles is initially uniform, convective instabilities may arise even when the applied temperature gradient 
  is below the threshold. This situation also occurs when in the sample there is already some solutal convective motion at the time when the temperature gradient is imposed. 
  In order to study this regimes, to better characterize the sample phase space, we have conducted some exploratory experiments according to the following procedure:
  \begin{itemize}
    \item The sample is heated from \emph{above} for 1 hour, applying a high temperature difference (we used $\Delta T = \SI{7.5}{\celsius}$ and 
          $\Delta T = \SI{10.5}{\celsius}$). This ensures that, because of the thermophilic behaviour  of LUDOX\reg TMA, the colloidal particles will diffuse
          towards the upper plate, and the sample will enter in a regime of solutal convection.
    \item The temperature gradient is abruptly reverted to a negative value \emph{below} the convective threshold. In our tests we used temperature 
          differences of $\Delta T = \SI{-3}{\celsius}$, $\Delta T = \SI{-3.38}{\celsius}$, $\Delta T = \SI{-3.75}{\celsius}$, $\Delta T = \SI{-4.13}{\celsius}$,
          $\Delta T = \SI{-4.35}{\celsius}$.
    \item shadowgraph images of the sample are recorded with a framerate of, approximatively, $10$ images/second.
  \end{itemize}
  \begin{figure}
  \centering
  \begin{subfigure}[]{0.32\linewidth}
    \includegraphics[width = \textwidth]{immagini/localized/start_a}
    \subcaption{}\label{subfig:start}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[]{0.32\linewidth}
    \includegraphics[width = \textwidth]{immagini/localized/solutal}
    \caption{}\label{subfig:sol}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[]{0.32\linewidth}
    \includegraphics[width = \textwidth]{immagini/localized/roll}
    \caption{}\label{subfig:roll}
  \end{subfigure}\\%
  \begin{subfigure}[]{0.32\linewidth}
    \includegraphics[width = \textwidth]{immagini/localized/rotating_a}
    \caption{}\label{subfig:rot}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[]{0.32\linewidth}
    \includegraphics[width = \textwidth]{immagini/localized/local1_a}
    \caption{}\label{subfig:loc1}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[]{0.32\linewidth}
    \includegraphics[width = \textwidth]{immagini/localized/local2_a}
    \caption{}\label{subfig:loc2}
  \end{subfigure}%
  \caption{Image sequence representing the evolution of the convective instability in a sample of LUDOX\reg TMA $4 \%$ w/w, heated from below, with $\Delta T = \SI{-4.13}{\celsius}$.
           At the beginning, heating from above with a temperature difference of \SI{10.5}{\celsius},   there is not macroscopic motion in the fluid. After some time, of the 
           order of tens of minutes, we have the development of solutal convection in the cell, as it can be seen in (b). After $1$ hour, the temperature gradient is inverted 
           and set to the regime, under threshold, value of \SI{-4.13}{\celsius}. In, approximatively, $3$ minutes, thermal convective rolls appear in the sample, (c). These 
           rolls, after some time, that depends on the Rayleigh number, start to rotate, (d), and to die out starting from the borders, (e), shrinking their size, (f), until 
           they finally disappear.}
  \label{fig:local6}
  \end{figure} 
  What happens under these conditions is that, when the sample is heated from above, at the beginning of the experiment, solutal convection destabilizes the suspension,
  so that, when the gradient is inverted, a convective instability develops in the sample, even when the sample is below the threshold for Rayleigh-Bénard convection. 
  The evolution of the instability can be appreciated looking at the sequence of images of~\cref{fig:local6}. At the beginning, the convective pattern are the usual straight 
  convective  rolls, spanning all the area of the cell. After some time, the patterns begin to rotate (clockwise or anticlockwise, indifferently) around the vertical axis. 
  Subsequently, convection begins to die out starting from the borders of the cell, so that there is a rotating localized pattern. Eventually, the rotating pattern shrinks 
  in dimensions, until it disappears completely and the sample reaches the stationary state. The duration of the entire phenomenon, measured from the inversion of the gradient, 
  is of the order of $1\div3$ hours. The same phenomenology is observed for all temperature differences studied, except for the largest one, $\Delta T = \SI{-4.35}{\celsius}$, 
  at which this instability has occurred only one time in three tests. What is happening, intuitively, is that there is a competition between the stabilizing effect of the 
  Soret effect and the convective motion of the initial conditions. When the system is below the convective threshold, the solute stabilizing effect eventually dominates and 
  the residual convection dies out. When, instead, the system is near, or above, the convective threshold, like in the case of $\Delta T = \SI{-4.35}{\celsius}$, 
  the instability lasts longer or, simply, the system enters the convective regime and the stabilization determined by the accumulation of particles at the bottom plate is 
  avoided because the strong convective flows prevent it by keeping the particles remixed. 

\begin{figure}
\centering
\begin{tikzpicture}[baseline, trim axis left]% 
  \begin{axis} [ xlabel = $Ra$,
                 ylabel = $t_\textup{ap} (s)$,
                 width = 0.57\linewidth]
      \addplot+ [color = teal,
                 mark options = {fill = teal},
                 error bars/.cd,
                 y dir=both,y explicit,
      ] coordinates {
          (2093.57,200)     +- (0,15)
          (2616.96,200)     +- (0,15)
          (3489.28,187)     +- (0,5)
          (3838.20,166)     +- (0,5)
       %   (4047.56,1)      +- (0.3,0.1)
      };
  \end{axis}%
 \end{tikzpicture}%
\hfill%
\begin{tikzpicture}[baseline, trim axis right]% 
  \begin{axis} [ xlabel = $Ra$,
                 ylabel = $t_\textup{rot} (s)$,
                 yticklabel pos=right,
                 width = 0.57\linewidth]
      \addplot+ [color = teal,
                 mark options = {fill = teal},
                 error bars/.cd,
                 y dir=both,y explicit,
      ] coordinates {
          (2093.57,200)     +- (0,15)
          (2616.96,1800)     +- (0,15)
          (3489.28,1740)     +- (0,5)
          (3838.20,2133)     +- (0,5)
       %   (4047.56,1)      +- (0.3,0.1)
      };
  \end{axis}%
 \end{tikzpicture}%
\vfill%
\vspace{3mm}%
\centering
\begin{tikzpicture}[trim axis left]% 
  \begin{axis} [ xlabel = $Ra$,
                 ylabel = $t_\textup{ex} (h)$,
                 width = 0.57\linewidth]
      \addplot+ [color = teal,
                 mark options = {fill = teal},
                 error bars/.cd,
                 y dir=both,y explicit,
      ] coordinates {
          (2093.57,0.94)     +- (0,0.004)
          (2616.96,1.39)     +- (0,0.004)
          (3489.28,1.69)     +- (0,0.001)
          (3838.20,2.67)     +- (0,0.001)
       %   (4047.56,1)      +- (0.3,0.1)
      };
  \end{axis}%
 \end{tikzpicture}%
\caption{In these  plots of the time of appearance of the convective rolls, $t_\textup{ap}$, the time of beginning of the rotation of the pattern, $t_\textup{rot}$, and the 
         time of extinction of the convective instability, $t_\textup{ex}$, are reported as a function of the Rayleigh number of the the sample. The error bars take into 
         account the number of measurements carried out with the same temperature differences. The values of the third and fourth points are the result of an average of three 
         measurements. In the second and third plot, the error bars are not visible because of the different timescale involved.}
\label{fig:plots-loc}
\end{figure}

  By loooking at sequences of images, we measured some charateristic quantities of the localized states. The interesting 
  quantitites are:
  \begin{itemize}
    \item  \emph{appearance time}, $t_\textup{ap}$, defined as the time after which thermal convection appears in the sample;
    \item  \emph{rotation appearance time}, $t_\textup{rot}$, defined as the time after which the convective patterns start to rotate;
    \item  \emph{extinction time}, $t_\textup{ex}$, defined as the time after which the convective patterns die out; 
    \item  \emph{roll wavelength}, $l_\textup{ROLL}$, defined as the length of the distance between two bright lines in the image, measured at the appearance time;
    \item  \emph{rotation velocity}, $l_\textup{ROLL}$, defined as the angular velocity of the rotation of the pattern, measured when the pattern still spans 
           all the cell.
  \end{itemize}
\begin{figure}
\centering
\begin{tikzpicture}[baseline,trim axis left]% 
  \begin{axis} [ xlabel = $Ra$,
                 ylabel = $ l_\textup{ROLL}(mm)$,
                 width = 0.57\linewidth]
      \addplot+ [color = teal,
                 mark options = {fill = teal},
                 error bars/.cd,
                 y dir=both,y explicit,
      ] coordinates {
          (2093.57,5.4291)     +- (0,1)
          (2616.96,5.969)     +- (0,1)
          (3489.28,5.832)     +- (0,0.3)
          (3838.20,5.740)     +- (0,0.3)
       %   (4047.56,1)      +- (0.3,0.1)
      };
  \end{axis}
 \end{tikzpicture}%
 \hfill%
\begin{tikzpicture}[baseline,trim axis right]%
  \begin{axis} [ xlabel = $Ra$,
                 ylabel = $ \omega_{rot}(rad/s)$,
                 yticklabel pos=right,
                 ymin = 0.006,
                 ymax = 0.015,
                 width = 0.57\linewidth]
      \addplot+ [color = teal,
                 mark options = {fill = teal},
                 error bars/.cd,
                 y dir=both,y explicit,
      ] coordinates {
          (2093.57,0.007853)     +- (0,0.00015)
          (2616.96,0.012566)     +- (0,0.0004)
          (3489.28,0.012477)     +- (0,0.0004)
          (3838.20,0.009773)     +- (0,0.0002)
       %   (4047.56,1)      +- (0.3,0.1)
      };
  \end{axis}%
 \end{tikzpicture}%
 \caption{Plots of the roll wavelength, $l_\textup{ROLL}(mm)$, and of the angular velocity of the rotation, $\omega_{rot}(rad/s)$. }
 \label{fig:plot-l}
 \end{figure}
  As origin for the time measurements we have taken the instant when the gradient get reversed, which is known with great precision. This measurements are made looking at 
  the image sequences to estimate the exact moments of interest. As the phenomenon is a continuous one, it is clear that these qualitative measurements have great 
  uncertainties, and their purpose is to have an idea of the dependence of the dynamics of the localized states on the Rayleigh number of the sample. The Rayleigh numbers
  corresponding to the temperature differences used (see above) range from about $2000$ to about $4000$. 


  The results are summarized in the plots of~\cref{fig:plots-loc}, for the times, and in~\cref{fig:plot-l} for the roll wavelength and the rotation velocity. As we can see, 
  according to these explorative measurements, it seems that $t_\textup{ap}$ slightly decreases as the Rayleigh number increases, while $t_\textup{rot}$ and $t_\textup{ex}$ 
  both grow as $Ra$ grows. $l_\textup{ROLL}$ and $l_\textup{ROLL}$, instead, don't appear to have a simple dependence on the Rayleigh number. The behaviour of the rotation 
  appearance time is of major interest, showing a large variation as a function of Rayleigh numbers, indicating that the range explored is extremely important and revealing. 
  Indeed, in this range, as already noted, there is a competition between the Rayleigh-Bénard thermal convection and the stabilizing 
  effect of the colloidal particles. This competition leads to the formation of travelling waves patterns, i.e., convective patterns that move horizontally in the sample,
  like waves in a medium, that, in a circular cell, become rotating patterns, because of the geometry of the borders. However, at the high end of the explored range,
  Rayleigh-Bénard convection prevails, and the stabilizing effect due to the accumulation of the colloid on the bottom of the cell is hampered by the continuous remixing of the
  particles. Under these conditions, the sedimentation of the particles is slowed down, and so are the appearance of travelling waves and the beginning of the rotation.
  

  The study of spatially localized states  is of great interest in the field of pattern formation~\cite{Lerman1993,Batiste2006,Descalzi2011,Mercader2011} and they appear in a great
  variety of physical and biological systems. In the case of localized stationary convective states, they take the name of \emph{convectons}, and there is a growing number
  of works studying them from a theoretical, computational and experimental point of view~\cite{Lerman1993,Batiste2005,Batiste2006,Mercader2008,Mercader2011,Smorodin2011}. 
  However, many of these works deal with binary fluids with a negative separation ratio, $\mathcal{S}$,  with a value between $-1$ and $0$, i.e, where the thermal 
  contribution to the density variation is greater than the solutal one. Our system has a separation ratio of $-3.6$, so that the solutal part is dominating. Indeed, 
  many of the solutions studied in the cited works are traveling waves, that don't die out, whereas the instability that we observed in the end disappear, because of 
  the stabilizing effect of the highly thermophilic particles.
  Furthermore, recent studies have outlined the potentiality of the bistability of nanofluids similar to the one we studied in this work in the developing of a way 
  to actively control heat transfer by switching between the conductive regime and the convective one (and vice-versa) exploiting the
  thermophilic behaviour of the nanoparticles~\cite{Donzelli2009,Bernardin2012}. 

  The outcome of this analysis is that, although the sample, when heated from below, can become unstable even below the threshold for the Rayleigh-Bénard convection, by 
  starting in a configuration where an equilibrium sedimentation profile is present inside the sample, the onset of convective motions can be avoided by gradually imposing 
  a temperature difference to it. Therefore this condition is suitable for the investigation of non-equilibrium fluctuations in the absence of convective motions 

  \section{NEFs in LUDOX\reg TMA}
  \label{sec:fluc}
    NEFs. We used the experimental methods and the analysis procedures described, respectively, in~\cref{chap:setup} and in~\cref{chap:an}. It is important to know the 
    characteristic times of the diffusion phenomena inside the sample, in order to set the right measuring time and the waiting time before starting the measure. Indeed, once 
    the sample cell is mounted, the distribution of the colloidal particles inside the cell is clearly not stratified, but, according to what said above, it is 
    important to start with this condition in order to avoid convective instabilities. The time needed for the stratified profile to form inside the cell is of the order of the
    diffusion time constant, $\tau_\textup{diff} = 1/\left(Dq^2\right)$. This time is dependent on the wavevector magnitude, $q$, of the concentration inhomogeneity of
    interest. In the case of the diffusion along the vertical axis, a limit to the smallest q (i.e. to the largest possible inhomogeneity) is given by the cell
    thickness, $d$, so that the characteristic time becomes $\tau_\textup{diff} = d^2/\left(\pi^2 D\right)$, the $\pi^2$ coming from the relation between $d$ and $q$.
    In our experiments $d = \SI{3.1}{mm}$ and  this time is approximatively $12$ hours. As a consequence, each measuring session is preceeded by this waiting
    time. 

    We started with measurements of the sample with a concentration of colloidal particles of $4 \%$ w/w. 
    Because of the high thermophilic behaviour of the LUDOX\reg TMA particles (we remind that, for this material, $S_T =  \SI{-4.7e-2}{\kelvin^{-1}}$), we expected that 
    we would have observed strong NEFs in the samples. As we will see in the following, this is not the case, opening the way for further
    studies. 
    
      \begin{figure}
      \centering
      \begin{tikzpicture}
        \begin{axis}[ xlabel = $t(s)$,
                      ylabel = $\sigma^2$,
                      clip marker paths=true,
                      legend entries = {data,moving average}]
          \addplot[color = teal, thin, only marks,mark = *, mark size = 1pt] table [x = t, y = var] {grafici/variance.dat};
          \addplot[color = orange,  thick ] table [x = t, y = mov] {grafici/variance.dat};
        \end{axis}
      \end{tikzpicture}
      \caption{Plot of the variance of the shadowgraph images of an experiment with LUDOX\reg \hspace{2pt}TMA with a concentration of $4 \%$ w/w and  $\Delta T = \SI{-3}{\celsius}$. The dots 
      are the data, the solid line is the moving average of the data.}
      \label{fig:plotvar}
      \end{figure}

    \subsection{The variance}
      The first analysis we carried out on the images series is the computation of the variance (computed pixel-wise) between the images and a reference image obtained 
      as the average of several images
      taken in equilibrium conditions, before the application of the temperature gradient. Looking at the trend of this quantity is a quick way to have an idea of the
      dynamics of the sample. In particular one can infer the development of the concentration gradient inside the sample. Furthermore, from the variance, eventual 
      disturbing phenomena, that could have ruined the measurement, are clearly visible as sudden jumps in the curve. We report the plot of one of the variance of one of the
      measurements in~\cref{fig:plotvar}. As we can see, there is a growth of the variance in the first part of the measurement, that slows down towards the end. It is 
      interesting to note that, after just $\sim \SI{5000}{\second}$, the variance has almost reached its maximum value, suggesting that the system has reached the stationary 
      state well before the predicted time of $12$ hours. %However, it has to be noted that, probably, the changes in light intensity caused by the increasingly small (as 
      %the time
     % passes and the concentration profile develops in the sample) rearrangements in 
     % the concentration profile are smaller than the noise. 
    
    \subsection{Transient process and dynamic analysis}
      From the variance of the time series  we found the period of time interested by the transient process that brings the system to the non-equilibrium steady state. 
      One of the goals of the work was to apply the dynamic analysis to this process to study the kinetics of growth of concentration non-equilibrium fluctuations. In order
      to do this, as briefly outlined in~\cref{sec:stat}, one has to split the time range in smaller intervals, to avoid breaking the ergodic hypothesis, and then
      join the results to obtain the full structure function. This splitting operation is particularly simple using the algorithm we developed, where the user chooses
      the time interval to which apply the analysis at runtime. 
      \begin{figure}
      \centering
      \begin{tikzpicture}
        \begin{axis}[ xlabel = $dt(s)$,
                      ylabel = $C_\textup{m}(dt)$,
                      clip marker paths=true,
                      legend entries = {$q = \SI{5.05}{\centi\metre^{-1}}$,$q=\SI{12.16}{\centi\metre^{-1}}$,$q=\SI{19.27}{\centi\metre^{-1}}$,$q=\SI{26.38}{\centi\metre^{-1}}$},
                      legend style = {at={(axis cs:20,191)},anchor=south east}]
          \addplot[color = teal, thin,mark = *, mark size = 1pt] table [x = dt, y = cm] {grafici/q1.dat};
          \addplot[color = DodgerBlue3,thin,mark = *,mark size = 1pt] table [x = dt, y = cm] {grafici/q2.dat};
          \addplot[color = Coral3,thin,mark = *,mark size = 1pt] table [x = dt, y = cm] {grafici/q3.dat};
          \addplot[color = LightSkyBlue4,thin,mark = *,mark size = 1pt] table [x = dt, y = cm] {grafici/q4.dat};
        \end{axis}
      \end{tikzpicture}
      \caption{Plot of the computed structure functions, $C_\textup{m}(q,dt)$,  of an experiment with LUDOX\reg TMA with a concentration of $4 \%$ w/w and  
      $\Delta T = \SI{-3.38}{\celsius}$. In the plot only the first $4$ $q$-values are represented, those corresponding to the biggest wavelengths. The very fast 
      growth of the curves show  the dynamics of the temperature fluctuations, that relax  in times of the order of seconds; on the other hand, it is clear  that the slower 
      dynamics of the concentration fluctuations is not captured by the experiment. The structure functions of the higher-$q$ modes, not represented here, show a completely 
      flat trend.}
      \label{fig:plotcm}
      \end{figure}
   
      However, before analysing the transient process, we analyzed the stationary 
      state, i.e. the time range where the gradient has fully developed and concentration NEFs caused by velocity fluctuations are present in the sample. What we found
      is rather surprising. We performed measurements with different values of the temperature difference applied to the cell, ranging from \SI{-3}{\celsius} to 
      \SI{-4.13}{\celsius}, and the structure functions calculated from the images series obtained share a common feature: they are quite flat apart from a very steep
      growth in the first few seconds, as a function of the delay $dt$. One of those measurements is represented in the plot of~\cref{fig:plotcm}, where the structure functions
      of the first $q\text{-values}$ are shown. 
      The first, quick growth, shows the dynamics of temperature non-equilibrium fluctuations. Indeed, the time scale needed to impose a steady temperature difference to the 
      sample is much shorter than that of diffusion, and is of the order of $\tau_\textup{t} = d^2/\left(\pi^2 D_T\right) \approx \SI{10}{\second}$.
      The absence, in the structure functions, of the expected, slower, exponential saturation, corresponding to the relaxation of concentration NEFs could, in principle, 
      have different reasons.

      First of all, there could be some problem in the implementation of the algorithm. To exclude this hypothesis, we tested our program against an extensive set of data 
      acquired under microgravity conditions during a thermodiffusion process in a polymer solution, in the framework of the GRADFLEX space experiment~\cite{Vailati2011}.
      The analysis of the structure functions of the GRADFLEX data obtained with the software developed in this thesis showed that they are fully compatible with those 
      determined independently by the GRADFLEX team. This consistence check allowed to verify that the developed software works properly.

      Another possibility is that, in the imposed conditions, the non-equilibrium signal is too weak with respect to the equilibrium one and the electronic noise, so that
      we are not actually seeing it. In the following, we will explore this second possibility in more detail.

    \subsection{Static analysis}
    \label{subsec:stati}
      To explore the second hypothesis, we decided to measure the static power spectrum of this sample, in stationary non-equilibrium conditions, using the static 
      procedure described in~\cref{sec:sq}. Indeed, measuring the static power spectrum, it is possible to see if we recover the expected $\sim q^{-4}$ scaling or not.
      If not, it means that we are not seeing the non-equilibrium signal. 
      
      We started with the same concentration of $4 \%$ w/w used in the dynamic experiments. The experimental procedure for this experiments is similar to the one employed
      for the characterization of the stability of the sample. The sample is uniformly heated at a temperature that is intermediate between the temperatures that will be 
      imposed to the plates in the second stage.
      Then, the desired temperature difference is applied gradually, with slow steps. Indeed, according to the observations
      of~\cref{sec:rotating}, to avoid convection it is sufficient to preserve a vertically stratified density profile. This can be attained heating very slowly, and 
      allows to reach temperature differences, and, consequently, temperature gradients, well above the convective threshold. In our experiments, we were able
      to reach values of the magnitude  of $\Delta T$ up to \SI{15}{\celsius}. Once the target $\Delta T$ has been reached, it is necessary to wait for a time of the order of 
      $\tau_\textup{diff}$, needed for the concentration gradient inside the sample to reach the stationary state. Then, the acquisition of statistically independent 
      shadowgraph images starts. The same procedure is then repeated in a separated experiment, but at constant temperature, to obtain the electronic background.
    
      Applying the static analysis procedure (see~\cref{sec:sq}) to the sequence of images obtained in this way, we recovered the static power spectra of the
      sample for the different $\Delta T$ investigated. The outcome of this analysis seems to go in the same direction of the second hypothesis we made in the previous
      section, that is the possibility that the non-equilibrium signal is too weak with respect to the equilibrium one and the noise. Indeed, we weren't able to recover the expected $q^{-4}$ scaling of the spectrum for any of the $\Delta T$ analyized, and the measured spectrum, 
      $S_\textup{m}(q)$ (see~\cref{eq:meas-spec2}), is often lower than the background spectrum, showing that the non-equilibrium signal is too small or that,
      anyway, it doesn't get measured with this procedure. As an example of this, the plots of $S_\textup{m}(q)$ and $B(q)$ of the experiment with 
      $\Delta T = \SI{-15}{\celsius}$ are  shown in the plots of~\cref{fig:plotsm}, where we can see that, for many wavevector values,the spectrum of the background, $B(q)$,
      has greater values than $S_\textup{m}(q)$.
      \begin{figure}
      \centering
      \begin{tikzpicture}[baseline, trim axis left]%
        \begin{loglogaxis}[ xlabel = $q (cm^{-1})$,
                      clip marker paths=true,
                      legend entries = {$S_\textup{m}(q)$,$B(q)$},
                      width = 0.57\linewidth]
%                      legend style = {at={(axis cs:20,191)},anchor=south east}]
          \addplot[color = teal, thin,mark = *, mark size = 0.4pt] table [x = q, y = sm] {grafici/sm.dat};
          \addplot[color = orange,thin,mark = *,mark size = 0.4pt] table [x = q, y = bm] {grafici/bm.dat};
        \end{loglogaxis}
      \end{tikzpicture}%
      \hfill%
      \begin{tikzpicture}[baseline, trim axis right]%
        \begin{loglogaxis}[ xlabel = $q (cm^{-1})$,
                      clip marker paths=true,
                      legend entries = {$S_\textup{m}(q)$-$B(q)$},
                      yticklabel pos=right,
                      width = 0.57\linewidth]
%                      legend style = {at={(axis cs:20,191)},anchor=south east}]
          \addplot[color = teal, thin,mark = *, mark size = 0.4pt] table [x = q, y = bm] {grafici/sb.dat};
        \end{loglogaxis}
      \end{tikzpicture}
      \caption{On the left, th plot of the measured power spectrum, $S_\textup{m}(q)$ and the electronic bacground, $B(q)$, for a concentration of $4 \%$ w/w and with 
               $\Delta T = \SI{-15}{\celsius}$. In many points, $B(q)$ is greater than $S_\textup{m}(q)$. On the right, the plot of the spectrum, $S(q)$, obtained as difference
               of $S_\textup{m}(q)$ and $B(q)$. }
      \label{fig:plotsm}
      \end{figure}
      
      To verify if the signal was too low because of the low concentration of the colloid, we conducted an experiment with a high concentration of $34 \%$ w/w. Indeed,
      it seems reasonable to expect that, since the static structure factor of the concentration NEFs,~\cref{eq:static-final}, is proportional to the concentration gradient,
      which is proportional to the concentration of the sample  because of the Soret effect, increasing the concentration of the sample would result in a greater 
      non-equilibrium signal. However, as mentioned in~\cref{sec:sample}, high concentrations may introduce other unwanted effects. Indeed, in the measurements carried out
      with these concentration we observed the formation of an annular pattern inside the cell (see~\cref{fig:blob}) that invalidated the measure. This phenomenon occurred
      in two different measurements, and its probably due to the gelation of part of the colloid. 
      \begin{figure}
        \centering
        \includegraphics[width = 0.77\linewidth]{immagini/blob}
        \caption{One of the images of the sequence of the experiment at a concentration of $34 \%$ w/w. Here the annular pattern is clearly visible.}
        \label{fig:blob}
      \end{figure}
      
    \subsection{Outlook and hypothesis}
    \label{subsec:casimir}
      To summarize the results of the experiments that we carried out in this work, we can affirm that the dynamic and static properties of the LUDOX\reg TMA 
      colloidal suspension under the conditions studied in this thesis, that encompass a significant range of Rayleigh numbers and concentration values, are different
      than expected. In particular, we didn't observe the expected exponential saturation in the structure function of the sample, and the measured power 
      spectrum isn't significantly different from the electronic background.

      In order to better  asses these results, more experiments with values of concentration different from those used in these experiments and possibly with different 
      colloidal samples are required.
      They will be important, also, to gain insight on the physical origin of the anomalous results here described. 
      
      A possible hypothesis, that,  at the present moment remains speculative, is that the non-equilibrium condition could effectively modify the interparticle
      interaction, making it repulsive. This phenomenon would hinder the motion of the particles, slowing down their dynamics and preventing their large scale
      distribution. If true, this situation would also affect the static behaviour of the sample. Indeed, the ``freezing'' of the dynamics implies the breaking of the 
      ergodicity, a hypothesis on which the analysis procedure used in this work is based on. A similar situation of quenching of the particles dynamics due to 
      an effective repulsive interaction has been predicted and observed in the so called \emph{colloidal Wigner glasses} and \emph{crystals}~\cite{Bonn1999,Russell2015}.
      Those, similarly to the low density electron gas that can form bound crystal states thanks to the presence of a long range interaction, from which they 
      take the name, are glassy (or crystal, depending on the colloidal particles concentration) bound states that may form in a colloid with long range repulsion 
      between the particles. In a system in non-equilibrium conditions, strong interactive forces may arise due to the confinement of fluctuations of temperature or concentration. 
      These Casimir-like forces have been theoretically predicted for temperature and concentration fluctuations (\cite{Kirkpatrick2013,Kirkpatrick2015}) and their measurement is one of the goals of 
      the space project of which this work is a part~\cite{Baaske2016a}. They arise because of the confinement of non-equilibrium fluctuations and should be  much stronger
      than the equilibrium ones, which become appreciable only close to a critical point. Although, at the present moment, this remain an hypothesis, the results that we obtained could be related to this effect. 
