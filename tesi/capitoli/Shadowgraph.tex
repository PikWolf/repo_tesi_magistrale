\chapter{The shadowgraph technique}
\label{chap:shadow}
  In~\cref{chap:NEF} we have outlined the theory of NEFs and, in particular, we have derived the static structure factor of the
  fluctuations, $S^{z,t}(q)$, and their relaxation time, $\tau_c$. The structure factor is a quantity directly accessible in 
  scattering experiments. Among the various available scattering techniques, the \emph{shadowgraph} technique represents a suitable alternative to traditional far-field scattering techniques for
  the study of NEFs in a colloidal suspension, as we will see in this chapter. 

  Traditionally, scattering experiments are made in \emph{far-field} conditions, where the scattered light is collected by a 
  detector placed at a large distance from the sample, if compared to the typical sample size.
  The main outcome of this approach is that the Fourier components of the scattered field get separated and the sensor records
  directly the structure factor of the sample. Indeed, the simple propagation, or the use of lenses, perform optically a Fourier
  transform on the scattered beam.  
  But this approach has also some drawbacks, the main ones being the 
  fact that this techniques require a very precise alignment of the optical elements and the fact that the transmitted beam needs
  to be disposed of to measure the scattered signal at low angles, and this operation usually affects the performance of the measurement
  for low wavevectors. 
  
  On the other hand, more recently developed \emph{near-field} techniques overcome these difficulties, and, among them, 
  the shadowgraph technique is suitable to study transparent samples with small refraction index variations~\cite{Cerbino2009}. In this chapter
  we will introduce the functioning principles of near-field scattering and, later, we will describe the quantitative 
  shadowagraph technique.

  \section{Near-field scattering}
  \label{sec:near}
    \begin{figure}[]
    \centering
      \includegraphics[width = 0.8\linewidth]{immagini/CerbinoVailati.jpg}
      \caption{Diffraction of light from a set of circular apertures. The images show numerical results of the near field intensity distribution evaluated at 
      increasing distance z from the plane containing the apertures. Only a small portion of the illuminated region is shown. The insets show the two-dimensional 
      spatial auto-correlation functions of the respective images. The distances are given as a function of the radius $R$ of the apertures and of the wavelength $\lambda$ 
      of light. (a) represents an image of the aperture plane ($z = 0$); (b) in a slightly out-of- focus image ($z = 0.25R^2/\lambda$) one can appreciate the 
      diffraction spots associated to each aperture; (c) at larger distance from the apertures ($z = 50R^2/\lambda$) the local superposition of light diffracted 
      by a sufficiently large number of apertures originates a speckle field. The size of the speckles coincides approximately with the diameter of the apertures; 
      (d) by doubling the distance ($z=100R^2/\lambda$), and still remaining in the near field region, no appreciable change of the speckle pattern is visible. 
      From the insets of the images one can obtain a more quantitative demonstration of the invariance of the correlation function upon propagation. In particular, 
      the width of the central peak is constant and coincides with the apertures' diameter. With courtesy of~\cite{Cerbino2009}}
      \label{fig:cerb-vail}
    \end{figure}
    As opposed to far-field, doing a near-field experiment means placing the detecting sensor at a distance from the sample 
    that is, in a way to be defined, ``small''. This regime has been investigated in a series of works in recent years, which have paved the way 
    to new quantitative experimental techniques that fill the gap between imaging techniques and far-field scattering 
    techniques~\cite{Giglio2000,Giglio2001,Brogioli2002,Trainoff2002,Cerbino2009,Croccolo2011,}.
   
%    The near-field approach is based on the Van Cittert and Zernike (VCZ) theorem, that states that the correlation function of the field scattered by 
%    a random sample at a certain distance is simply given, up to multiplicative constants, by the Fourier transform of the intensity distribution 
%    of the sample~\cite{Giglio2000,Giglio2001}. 
    
%    If we take the observation plane $x-y$ (a plane, perpendicular to the optical axis that coincide with the focal plane 
%    of the recording device)) at a distance, $z$, from the sample, $\m{r} = (\Delta x, \Delta y) $, with $\Delta x = x_1 -x_2$ and $\Delta y = y_1 -y_2$, the, 
%    if $\m{q}$ is the scattering wavevecto

    When studying liquids, macromolecules, gels, porous materials and complex fluids with scattering in the far-field, the intensity of the scattered 
    radiation $I(\theta)$ is recorded as a function of the scattering angle $\theta$, that is the angle between the direction of the incoming beam and 
    the direction of the scattered beam (see~\cref{subfig:candle}). The intensity in the far-field is given by the superposition of the fields scattered by different portions of the
    sample, with different phases, and this gives rise to coherence areas called \emph{speckles}. According to the Van Cittert and Zernike theorem~\cite{Giglio2000},
    the size and shape of the speckles in the far-field are related to the intensity distribution of the probe beam, but there is no relation between the statistical
    properties of the speckles pattern and the physical properties of the sample. Intuitively, this is because, as said, at a great distance from the sample, the signal is a 
    superposition of the contributions coming from every portions of the sample, and every information about the local arrangement of the scatterers is lost. 
    Conversely, when one puts the detector near the sample, so that the radiation impinging on it comes only from a portion of the sample, and not from all the 
    illuminated portion, the statistical properties of the recorded intensity is related to the local structure of the sample~\cite{Giglio2000,Giglio2001,Brogioli2002}. 
    Indeed, considering the scheme of~\cref{fig:nfs}, if $d$ is the dimension of the scatterers, and $D$ the dimension of the illuminated area, a condition for the distance 
    $z$ from the sample at which this happens, involving only these quantities and the wavelength of the incoming radiation, $\lambda$, can be derived~\cite{Giglio2001}:
    \begin{equation}
    \label{eq:NF-cond}
      z \ll \frac{Dd}{\lambda}
    \end{equation}
    \begin{figure}
    \centering
      \includegraphics{immagini/nfscheme}
      \caption{In near-field, only the light scattered by a portion of the illuminated area, $D$, impinges on the sensor.}
      \label{fig:nfs}
    \end{figure}
    In this region, called the \emph{near-field region}, the transverse correlation properties of the intensity field, i.e. the spatial correlation function on the
    planes perpendicular to the optical axis, are invariant upon propagation, as shown in~\cref{fig:cerb-vail},~\cite{Cerbino2009}. This fact makes these techniques 
    more agile from an experimental point of view, allowing to place the sensor in a certain range of distances from the sample. 

    There are two possible schemes for the near-field technique: the \emph{homodyne detection}, where the transmitted beam needs to be removed, and only the interference
    between the scattered waves contributes to the signal, and the \emph{heterodyne detection}, where, instead, the transmitted beam is not removed and takes the 
    role of a local oscillator. In the following, we will describe the heterodyne scheme, as this is the one used in this work. 
    
    A pictorial representation of the minimal heterodyne setup is reported in~\cref{fig:heter}. It consists of the light source, the sample and a sensor that can record
    light intensity (typically a CCD sensor) placed in the near-field region. The scattered beams interfere with the transmitted beam (and with each other, but, as we will
    see, in weak scattering condition, this contribution is negligible) and the interference pattern gets recorded. The properties of this pattern are related
    to the properties of the sample. In particular, if $\m{x}$ is a point of the pattern, and $\m{q} = \m{k_\textup{s}} - \m{k_\textup{i}}$, where $\m{k_\textup{s}}$ and 
    $\m{k_\textup{i}}$ are, respectively, the incident and the scattered radiation wavevectors (see~\cref{subfig:candle}), 
    is the scattering wavevector, then, if 
    $U_\textup{S}(\m{x},t)$ is the scattered field and $g_{U_\textup{S}}(\m{x},t) = \left<  U_\textup{S}(\m{x},t) U_\textup{S}(0,t)\right> $ is the 
    two-dimensional spatial auto-correlation function, the following relation holds:
    \begin{equation}
    \label{eq:near-kin}
      i_\textup{S}(\m{q},t) = \left| U_\textup{S}(\m{q},t) \right|^2 = \int g_{U_\textup{S}}(\m{x},t) \mathrm{e}^{-i \m{q} \cdot \m{x}} d\m{x}
    \end{equation}
    \begin{figure}[]
      \centering
      \includegraphics[width = \linewidth]{immagini/NearField.eps}
      \caption{Pictorial representation of a near-field scattering experiment. The interference fringes between the scattered light and the transmitted beam 
      are collected by a detector in the near-field region.}
      \label{fig:heter}
    \end{figure}
    where $i_\textup{S}(\m{q},t)$ is the scattered light distribution and the second equality is the Wiener-Kintchine theorem, that says that the relation
    between the auto-correlation function and the fourier components of a fluctuating field is simply a Fourier transform. This is very important, since it tells us that
    we can obtain the structure factor of the sample, wich, we recall (see~\cref{sub:struct}), it's related to the autocorrelation function of the density of the sample,
    by measuring the power spectrum, i.e. the square modulus of the Fourier decomposition of the intensity, of the light in a near-field scattering experiment. 
    In a scattering experiment, what gets measured is the intensity of the radiation, and not the field directly. In experiments involving soft matter samples,
    like fluids, colloidal suspension, gels and so on, the samples are usually weakly scattering and most of the radiation pass through them unaltered. This enables to
    use the weak-scattering approximation, i.e. the amplitude of the scattered field is much lower than the amplitude of the transmitted beam, $U_0$: $U_\textup{S} \ll U_0$.
    In this way, the total scattered light intensity is given by~\cite{Cerbino2009}:
    \begin{equation}
    \label{eq:scat-int}
      I_\textup{tot} (\m{x},t) = \left| U_0 + U_\textup{S}(\m{x},t)  \right|^2 \approx \left| U_0 \right|^2 + 2\operatorname{Re}\left[ U_\textup{S}(\m{x},t) U_0^* \right]
    \end{equation}
    obtained neglecting the term $\left| U_\textup{S} \right|^2$. The signal of interest is contained in the second term, but the term $I_0 = \left| U_0 \right|^2$, being 
    constant, can be easily removed during the analysis of the data, as we will see in~\cref{chap:an}, by averaging on all the images taken. 

  \section{The shadowgraph}
  \label{sec:shado}
    There are many kinds of near-field heterodyne techniques, each one with it's own advantages and disadvantages, depending on the application. 
    In this section the \emph{shadowgraph technique}, used in this work, will be described. Before going into the details, the most immediate way to 
    understand what is shadowgraphy is with an example. Imagine to have a candle and to illuminate the plume with a light source. If one puts a screen behind it, a 
    \begin{figure}[]
      \centering
      \begin{subfigure}[T]{0.4\linewidth}
        \includegraphics[width = \linewidth]{immagini/candleplume1}
        \subcaption{}\label{subfig:scheme}
      \end{subfigure}%
      \hfill%
      \begin{subfigure}[T]{0.55\linewidth}
        \includegraphics[width = \linewidth]{immagini/wavevector}
        \captionsetup{skip=15pt}
        \subcaption{}\label{subfig:candle}
      \end{subfigure}%
      \caption{In (a), a very simple shadowgraphy. The shadow pattern visible in both the pictures is the result of the interference of light rays deflected by the 
      refractive index fluctuations in the region above the flame.~\cite{Hargather2009}. In (b), the scheme of the the scattering wavevector, $\m{q}$.}
      \label{fig:scheme}
    \end{figure}
    sort of shadow projection will form on it (see~\cref{subfig:scheme}). This is a very simple and raw shadowgraphy. What is happening is that the hot air flow above the flame causes local 
    inhomogeneities in the refraction index, so that when light rays go through that region they get deflected and interfere with each other, resulting in the pattern 
    that we see on the screen. For this reason, shadowgraphy has always been used in tasks such as 
    flow visualization in the study of the fluid mechanical behaviour of transparent media. However, this technique can also be quantitative, as we will see. 
    
    Shadowgraphy shares several theoretical and experimental aspects with another technique, the Schlieren technique (from which, historically, shawdography derives), that 
    we will briefly outline. Both of these techniques are near field scattering techniques, and their optical setup is almost the same. In~\cref{fig:shadow-opt}, the optical
    scheme of the shadowgraph is illustrated. It is made by:
    \begin{itemize}
      \item a light source;
      \item an achromatic doublet, to turn the source beam into a parallel beam;
      \item the sample;
      \item a convergent lens;~\footnote{The convergent lens is not strictly necessary. Shadowgraph experiments can be made with the direct projection of light, but we reported here the configuratin we used in this work.}
      \item a screen or a detector where the scattered patterns are recorded.
    \end{itemize}
    The only difference in a Schlieren setup is the presence of an intensity mask (for example, a razor blade) that performs some spatial filtering of the outgoing light, 
    positioned in the Fourier plane of the convergent lens.  
    At a first order approximation, we can assume that, for each wavevector $\m{q}$ of the sample refraction index fluctuations, there will be two small amplitude first-order
    beams (see the inset of~\cref{fig:shadow-opt}), scattered at angles $\pm\theta \approx \pm 2 \arcsin \left( \frac{\lambda q}{4 \pi} \right)$. 
    The role of the intensity mask in the Schlieren approach is 
    to cancel one of the two specular scattered beams, so that the measured pattern is simply the intensity of the primary beam plus a small amplitude flutcuating pattern
    due to the fluctuations of the refractive index. In the shadowgraph, instead, the measured pattern is the superposition of the primary beam and the two specular 
    scattered beams, for each wavevector. The two beams interfere with the primary beam, originating two sinusoidal intensity distributions. The two intensity distributions
    correspondig to the two waves with wavevectors $\pm k_\textup{s}$, sum on the observation plane. Each one of the two intensity distributions has maxima and minima that 
    depend on the value of $q$. For certain values of $q$ the maxima of one of the two distribution are in correspondence with the minima of the other one, so that the 
    resulting intensity distribution is flat. For other values of $q$ the maxima of the two distributions correspond. This mechanism introduces a modulation in the shadowgraph 
    sensitivity. In this way, we see the differences of the two techniques: the shadowgraph technique
    is much simpler, because it doesn't require a very precise alignment, but this comes at the cost of a sensitivity that is not constant, but has oscillations depending
    on the wavevector and the distance; the Schlieren does not have this problem but requires a very precise alignment. The modulation in the sensitivity can be modeled
    introducing the \emph{transfer function} $T(q)$, such that the measured power spectrum of the shadowgraph images (we recall that the power spectrum is the the square 
    modulus of the intensity Fourier transform and it's proportional to the static structure factor of the fluctuations) can be written as:
    \begin{figure}[tb]
      \includegraphics[width = \textwidth]{immagini/schema_shadow.eps}
    \caption{Optical setup of the shadowgraph. The lengths p, q and f follow the usual relation for the lens: $\frac{1}{f} = \frac{1}{p} + \frac{1}{q}$. In red is represented
    the intensity mask (a razor blade), which is present in the case of a Schlieren setup. In the inset it is
             represented the working principle of the shadowgraph, at the first order.}
    \label{fig:shadow-opt}
    \end{figure}
    \begin{equation}
    \label{eq:meas-sp}
      S_\textup{m} (\m{q}) = T(\m{q})S(\m{q}) + B(\m{q})
    \end{equation}
    where $B(\m{q})$ is  a term accounting for the noise coming from the measuring device, called \emph{electronic background}.
    It can be shown (see, for example,~\cite{Croccolo2011} or~\cite{Trainoff2002}) that the general analytical form of $T(q)$ for the case of Schlieren and shadowgraph is:
    \begin{equation}
    \label{eq:tq}
      T(\m{q}) = \left| f(\m{q}) \mathrm{e}^ {i\frac{q^2z}{2k_0}} - f(-\m{q}) \mathrm{e}^ {-i\frac{q^2z}{2k_0}} \right|^2
    \end{equation}
    where $f(\m{q})$ is the transmission function of the mask positioned in the focal plane of the lens. For the Schlieren, the mask is a razor blade put in half of the
    Fourier plane, so $f(\m{q})$ is the step function:
    \begin{equation}
    \label{eq:mask-s}
      f(\m{q}) = \chi_{q_x} [0,\inf]
    \end{equation}
    with $\chi_{q_x}$ the Heaviside function in the bidimensional dual space $(q_x,q_y)$. We thus obtain, as anticipated, $T(\m{q}) = 1$ for the Schlieren. For the 
    shadowgraph, there is no mask, so:
    \begin{equation}
    \label{eq:mask-sh}
      f(\m{q}) = 1, \,\,\, \forall \m{q}
    \end{equation}
    As a consequence, the transfer function of the shawdograph technique is:
    \begin{equation}
    \label{eq:transf-teo}
      T(\m{q}) =  \sin^2 \left( \frac{q^2z}{2 k_0} \right)
    \end{equation}
    Actually, this analytical form is obtained under the assumption of an infinitely thin sample, and it  captures well the oscillating 
    behaviour of the experimental transfer function as a function of the wavevector and the visualization distance, $z$,  but misses the dampening of the oscillations
    at great $q\text{-values}$, that is observed experimentally. For this reason, empirical forms of $T(\m{q})$ obtained from experimental calibrations are usually used 
    to fit the data. 
    An example of such an empirical form, derived by Croccolo et al.~\cite{Croccolo2013},  is:
    \begin{equation}
    \label{eq:transf-emp}
      T(\m{q}) = \frac{1}{2} + \left[ \sin^2 \left( \frac{q^2z}{2 k_0} \right) - \frac{1}{2} \right] \left\{ \exp \left[ - \left( \frac{q}{q_\textup{thick}} \right)^{\gamma} \right]
             \right\}
    \end{equation}
    where $q_\textup{thick}$ is a typycal wavevector related to the disappearance of transfer function oscillations and $\gamma$ is an exponent describing how fast
    the oscillations die out (see the plot of~\cref{fig:transferF}). Other examples can be found in literature~\cite{Vailati2011}. A general theoretical derivation for the 
    transfer function is yet to be found, and the difficulty lies in the fact that for each different experimental setup, the function is different. 
    \begin{figure}[htp]
      \centering
          \begin{tikzpicture}%
            \begin{loglogaxis} [ xlabel = $q$,
	                               ylabel = $T(q)$,
                                 xmin = 0.1,
                                 xmax = 100,
                                 width = 0.6\linewidth ]%
              \addplot[domain = 0.1:1000, samples = 1000, smooth, teal] {1/2 + ((sin(5*x^2))^2- 1/2)*(exp(-(x*0.05))^0.8)};
	          \end{loglogaxis}%
          \end{tikzpicture}%
    \caption{Plot of the transfer function $T(q)$ of~\cref{eq:transf-emp}.}
    \label{fig:transferF}
    \end{figure} 

    \subsection{Machine Learning and shadowgraphy}
    \label{subsec:ML}
      During the progress of this work, we investigated the possibility of applying the tools offered by the machine learning (ML) field to the problem of 
      retrieving the transfer function of the shadowgraph without knowing its exact analytical form and without carrying out separated ad hoc experiments to measure it. 
      ML is a growing field of research and its tools are widely adopted in many fields, such as informatics, engineering, life sciences and in industry. The goal of 
      ML is to build statistical models and procedures (to be translated into computational algorithms) that are able to learn information from data and generalize 
      the information learned to make predictions on new data. The principal 
      tasks where this approach is successful are:
      \begin{itemize}
        \item regression problems;
        \item clustering;
        \item classification problems;
        \item pattern recognition, and, in particular, image recognition.
      \end{itemize}
      There are two main categories of learning algorithms: \emph{supervised learning} and \emph{unsupervised learning}. The algorithms belonging to the first category
      are \emph{trained} using a dataset made by pairs of data samples: an input sample, which is the type of data that the model has to learn to classify, or recognise, 
      and the target sample, which is the known, correct information, class or feature corresponding to that particular input sample. In the training procedure, the algorithm 
      ``looks'' at all of the pairs of this dataset, adjusting the model internal parameters so that, when the model is prompted with one of the training input sample,
      is able to give the correct output with a certain degree of accuracy. One of the greater risks of this approach is the problem of overfitting. This happens when
      the model has too many parameters and they get adjusted too much on the training dataset: in this way the model predict perfectely every target of the training 
      dataset but is not able to generalize to new data. To avoid overfitting, it is customary to add a \emph{validation} phase. This consists in building a second dataset,
      analogous to the training one, but with different data, that is used to check the accuracy of the predictions of the model \emph{as} the training goes on~\footnote{It
      has to be noted that, in real applications, not always additional data is available. In this case the training and validation dataset are built by splitting the
      available dataset in two subsets.}. 
      In this way,
      one has an indication on when to stop the training phase. Indeed, in principle, it is possible to train the model indefinitively, and attain a great accuracy on the
      training dataset, but, as already said, this inevitably leads to overfitting.  
      The unsupervised algorithms, as the name suggest, don't rely on the previous knowledge of several known examples, like the supervised ones. Instead, they try 
      to learn the features of the data leveraging the statistical correlations between them. A typical example of problems tackled by unsupervised algorithms is
      represented by the clustering problems. This consists in finding algorithms that are able to group a set of object in such a way that objects in the same group 
      are more similar (where the similarity measure depend on the specifical problem) to each other than to those in other groups. For example, in the context 
      of social networks, clustering techniques are used to recognize communities within large groups of people.
      
      In our case, the problem of interest is best suited to be solved with a supervised technique. It can be summarized as follows. We have a set of data, that are the power 
      spectra $S_\textup{m}(q)$, coming from the analysis of the shadowgraph images. These are one-dimensional functions of 182 points. It would be useful  to obtain, starting 
      from these functions, the spectra $S(q)$, which we remind being related to $S_\textup{m}(q)$ through the equation (we drop the vector notation from $q$ because here 
      we assume isotropy in the system):
      \begin{equation}
      \label{eq:meas-spe3}
        S_\textup{m} (q) = T(q)S(q) + B(q)
      \end{equation}
      In other words, we would like to get rid of the modulation introduced by the transfer function without actually characterizing it with
      separate measurements. This kind of problem falls within the class of problems called \emph{inverse problems}. In general, one has the output of a physical system
      that operates a transformation (usually some sort of unwanted filtering, like in our case) on the input, and wants to determine the input. That is,
      if $x$ is the input and $y$ the output:
      \begin{equation}
      \label{eq:inverse}
        y = f(x) + \epsilon
      \end{equation}
      with $f$ being the transformation applied to the input, and $\epsilon$ some eventual noise, and we want to retrieve $x$. As we can see, this equation is equivalent
      to~\cref{eq:meas-spe3}, that is our problem. These kind of problems can be solved, in the context of ML, building a supervised model able to learn the characteristics 
      of $f$ by training the model on datasets made by known $(x,f(x))$ pairs. In particular, deep  neural networks are suitable for this task, as, in principle, they can represent 
      any continuous function~\cite{Hornik1991,Leshno1993}, with only one hidden layer.  
      
      The model  we built (represented in~\cref{fig:model}) is a feed-forward neural network, made by an input and an output layer, each one made of 182 linear units, and one hidden layer made by 13 units 
      with sigmoid activation function. The linear units output is simply a sum of all the inputs, weighted on the weights of the links coming from the previous layer, that
      are the tunable parameters of the model. In the hidden layer, every unit performs a transformation on the weighted sum, applying a sigmoid function to it, i.e., if 
      $o_i$ is the output of the $i$-th hidden unit, $l_j$ are the outputs of the previous layer and $w_j$ are the weights associated to the incoming links from the 
      previous layer, then:
      \begin{equation}
      \label{eq:sigmoid}
        o_i = \text{Sigmoid} \left( \sum_j w_j l_j \right) = \frac{1}{1+ e^{-\sum_j w_j l_j}}
      \end{equation}
      \begin{figure}
      \centering
        \includegraphics[width = \linewidth]{immagini/MLP}
      \caption{Pictorial representation of the model.}
      \label{fig:model}
      \end{figure}
      
      To simplify the problem, we neglected the backroung noise $B(q)$, so that the problem reduces to recover $S(q)$ starting from $S_\textup{m} (q) = T(q)S(q)$.
      Because we wanted to test the goodness of this approach to solve this particular problem, as a proof of concept, we trained the net with synthetic data,
      generated by us. This has the advantage that the statistical properties of the data are totally under control, and allows to arbitrarily chose the parameters with
      which the data are built. In particular, we built a training dataset made by $5000$ pairs (input,target) where the input is a realization of $S_\textup{m} (q)$ 
      and the output the corresponding $S(q)$. Each realization of $S(q)$ is obtained starting from a random $256\times256$ image, where each pixel is extracted from a 
      Gaussian distribution with zero average. The square modulus of the FFT of the image thus obtained is multiplied with another image that is a filter on the 
      $q$-values whose pixels take the value $\frac{1}{1+(q/q_\textup{RO})^{-4}}$, with $q$ being the distance from the centre of the image. Taking the azimuthal average of the image obtained in 
      this way, we have the input of the data pair. To obtain $S_\textup{m} (q)$ we build the image corresponding to $T(q)$ using
      a simplified version  of~\cref{eq:transf-emp},
      \begin{equation}
      T(q) = \frac{1}{2} + \left[ \sin^2 \left( c_1 q^2 \right) - \frac{1}{2} \right] \Bigl\{ \exp \left[ - \left(c_2  q \right)^{\gamma} \right]
             \Bigr\}
      \end{equation}
      and then multiply it with $S(q)$. Therefore, the free parameters of the dataset thus obtained are:
      \begin{itemize}
        \item $q_\textup{RO}$, that specifies the position of the knee in the power spectrum $S(q)$. It is the most important feature of $S(q)$.
        \item $c_1$, $c_2$, and $\gamma$, in $T(q)$. These control, respectively, the position of the first oscillation peak, the decay length and how much the decay is 
              enhanced or slowed down.
      \end{itemize}
      We randomly picked the value of the parameters in certain ranges (chosen so that the obtained spectra resemble the experimental ones), in order to obtain a 
      more general dataset. An example of a pair of data of the dataset can be seen in~\cref{fig:pair}. We then generated other two analogous datasets, of $2000$ images each: one is the validation dataset and the other is the \emph{testing}
      dataset, used to test the performance of the model, i.e. the accuracy of the prediction on new data.
      \begin{figure}
      \centering
        \begin{subfigure}[]{0.5\linewidth}%
          \includegraphics[width =\linewidth]{immagini/Theproblem.jpg}
          \subcaption{ }
          \label{fig:pair}
        \end{subfigure}%
        \hfill%
        \begin{subfigure}[]{0.5\linewidth}%
        \includegraphics[width = \linewidth]{immagini/pred1}
        \subcaption{ }
        \label{fig:pred}
        \end{subfigure}%
        \caption{In (a), a data pair of the training dataset. In blue the target spectrum $S(q)$; in orange, $S_\textup{m} (q) = T(q)S(q)$. In (b), a prediction of the model. In orange the input spectrum; in blue the target spectrum; in green the predicted one. As it can be seen, the shape of the function and
        the position of the knee are well reconstructed. }
      \end{figure}

      The spectra  obtained in this way (as well as the experimental ones) span more than five orders of magnitude, and this represents a problem in the learning
      process, since the features of the high-$q$ values of the spectra contribute in a different way of the low-$q$ ones to the calculation of the error function (that is the function
      that has to be optimized, as a function of the parameters of the model, in the learning process). Furthermore, we are most interested in retrieving, among the other 
      characteritics of the spectra, the position of the knee, i.e. $q_\textup{RO}$. To solve this problem, we used a custom loss function, that introduces some
      weights favouring the learning of the interesting features by assigning more weight to the output nodes corresponding to the first decade of $q$-values, range within
      which usually there is $q_\textup{RO}$, and a gradually decreasing weight to the output coming from the subsequent nodes).

      We found that the model built and trained as described above is able to retrieve fairly well the position of the knee and the general shape of the spectra $S(q)$ starting
      from the modulated spectra $S_\textup{m} (q)$. An example of a prediction made with our model can be seen in~\cref{fig:pred}. We also analyzed the performance of the
      model varying the parameters of the testing dataset, looking at the correlation between the accuracy of the predictions and the parameters of the testing dataset, and 
      plotting the error on the prediction as a function of one parameter at a time, averaging on the other parameters (see~\cref{fig:analyserror}). From this analysis we found that the performance
      of the net is better for low values of $q_\textup{RO}$ and $\gamma$ and high values (in the ranges we used) of $c_2$. Instead, the error does not show a clear dependence
      on the value of $c_1$.
      
      
      The good results obtained with this rather simple toy model suggest that this and similar ML techniques could be succesfully applied to the field of experimental soft matter physics as
      useful methods to clean and interpret light scattering data. The next steps in the development of this approach will be introducing the background term in the synthetic datasets and, then, testing the model on real experimental data. 

  \begin{figure}
  \centering
  \begin{subfigure}[]{0.5\linewidth}
    \includegraphics[width = \textwidth]{immagini/error_q_ro}
    \subcaption{$q_\textup{RO}$}\label{subfig:qro}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[]{0.5\linewidth}
    \includegraphics[width = \textwidth]{immagini/error_c_1}
    \subcaption{$c_1$}\label{subfig:c1}
  \end{subfigure}\\%
  \begin{subfigure}[]{0.5\linewidth}
    \includegraphics[width = \textwidth]{immagini/error_c_2}
    \subcaption{$c_2$}\label{subfig:c2}
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[]{0.5\linewidth}
    \includegraphics[width = \textwidth]{immagini/error_gamma}
    \subcaption{$\gamma$}\label{subfig:gamma}
  \end{subfigure}%
  \caption{Plots with the error on the predictions of the net as a function of the parameters of the data.}
  \label{fig:analyserror}
  \end{figure}
