\chapter{Experimental methods}
\label{chap:setup}
  In the present work we studied a colloidal suspension with the shadowgraph technique described in~\cref{chap:shadow}. The suspension is kept in a non-equilibrium 
  condition by imposing a temperature gradient to it. In this chapter the experimental setup used in the experiments will be described. 
  
  \section{The experimental setup}
  \label{sec:app}
    As seen in~\cref{sec:near}, the experimental setup for a near-field measurement is quite simple, and it may simply consist of a light source, a detecting system
    and a sample cell.
    In the study of NEFs obtained exploiting thermodiffusion, the central element of the apparatus is a thermal gradient cell. This consist of a thin (with 
    respect to the other dimensions) layer of fluid confined by two transparent plates through which a thermal gradient can be applied. In our setup, the cell is made
    by two circular sapphire plates spaced by an o-ring, that confines  the fluid  inserted with the help of two needles. The heating elements are two ring-shaped 
    Peltier elements, devices that  work as heat pumps using the thermoelectric effect. The Peltier elements are thermally coupled to the sapphire plates using 
    a heat-sink paste. Furthermore, to work properly, the other side of both the elements must  be in contact with a thermal reservoir, consisting of two aluminium chambers 
    (one above, the other below) inside which there is a steady flow of water at constant temperature. The purpose of this reservoir is to remove the excess heat from the 
    cooling Peltier and to provide the required heat to the heating one. Indeed, in our configuration, the Peltier under the cell generates a positive heat flux at the surface 
    of the sapphire plate, while the other generates a heat flux from inside the cell to the outside, actually cooling the upper sapphire plate. This mechanism, illustrated
    in~\cref{fig:cell}, allows the creation of a steady temperature gradient inside the cell. Since, for the experiment, the important quantity is the temperature 
    difference (that gives rise to the gradient) between the two plates, and not the absolute value of the two temperatures, in principle one can arbitrarily choose the 
    reference temperature of the water~\footnote{Of course, the temperature must be between the fusion and the boiling point for water.}.  For the Peltier pair to work 
    properly it is usual to set it  at an intermediate value between the desired temperatures of the plates. Usually, it is convenient to set a reference temperature that 
    is approximatively the room temperature, because it requires less power to be maintained by the thermal bath. In~\cref{subsec:temp}, the procedure to control the 
    temperature and obtain a steady gradient inside the cell will be described.
    \begin{figure}[]
    \centering
      \begin{subfigure}[]{0.51\textwidth}
        \vspace{30pt}
        \includegraphics[width = \linewidth]{immagini/cell}
        \captionsetup{skip = 30pt}
        \subcaption{Schematics of the thermodiffusion cell used in this work.}\label{subfig:cell}
      \end{subfigure}%
      \hspace{3.2pt}%
      \begin{subfigure}[]{0.48\textwidth}
        \includegraphics[width = 0.9\linewidth]{immagini/cell_pic}
        \subcaption{Picture of the cell portrayed during the filling operation.}\label{subfig:cell-pic}
      \end{subfigure}%
      \caption{The thermodiffusion cell. Schematics (left) and a picture of the flanges holding together the sapphire windows.}
      \label{fig:cell}
    \end{figure}

    The cell is mounted on a stand and is illuminated by a superluminous diode (Superlum SLD-261) with central wavelength \SI{670}{nm}. Having a temporal choerence 
    length that is shorter than the one of a laser, this light source is more suitable for this experiment. Indeed, with a highly coherent light source, there would be 
    interference between the multiple reflections of the beam  on the surfaces of the cell windows. The optical setup used
    in our experiment is exactly that of~\cref{fig:shadow-opt}. The sensor employed is a monochrome CCD Camera (JAI CV-M300). 
    The convergent lens, placed between the sample cell and the camera, 
    has a focal length of \SI{30}{cm}. The distance $z$ is, in our case, of \SI{-90.6+-0.5}{\centi\metre}
    This is obtained measuring the magnification $m = -q/p$, where $q$ is fixed by the distance between the camera and the lens, that in our setup is of 
    \SI{34.5 +- 0.5}{\centi\metre}, and the distance, $d$, between the sample cell and the convergent lens. Being $d = p + z$, it is easy to find $z$. $m$ is obained 
    measuring, with an image manipulation software (like, for example, ImageJ) a known reference distance in the recorded images and knowing the mm/pixel ratio of the 
    camera sensor. 
  
  \section{The sample}
  \label{sec:sample}
    The sample chosen for the experiment is a colloidal suspension made by distilled water and LUDOX\reg TMA, a commercial colloid made of 
    silica nanoparticle with an average diameter of \SI{22}{nm}. This colloid has a negative Soret coefficient, that means that the colloidal 
    particles are thermophilic. The choice for this material has been driven by the fact that, as can be seen in~\cref{tab:ludox}, its thermal diffusivity
    is quite high, which should result in strong NEFs. In this work we have used concentrations of $4 \%$ w/w  and  $34 \%$ w/w. Changing the concentration
    of the colloidal particles has effects on the thermophysical properties of the sample. In particular, one has to take in consideration the fact that, during the
    imposition of the thermal gradient, there are layers of fluid where the concentration is much higher than the average concentration, because of the concentration
    gradient that forms for the Soret effect. Thus, when using high concentrations, there is the risk that in some place the colloid will exceed the gelation threshold,
    forming local aggregates. On the other hand, higher concentrations correspond to a stronger Soret effect, resulting in greater concentration gradient and, consequently,
    an enhancement in the intensity of the non-equilibrium fluctuations signal. 
    \begin{table}
    \centering
      \caption{LUDOX\reg TMA thermo-physical properties~\cite{Donzelli2009}.}
      \begin{tabular}{cS}
        \toprule
          Quantity            &     {Value}                             \\
        \midrule
          particle diameter   &     \SI{22}{nm}                         \\
          $D$                 &     \SI{2.2e-7}{\centi\metre^2/s}       \\
          $D_T$               &     \SI{1.52e-3}{\centi\metre^2/s}      \\
          $\beta_T$           &     \SI{2.97e-4}{\kelvin^{-1}}          \\ 
          $\nu$               &     \SI{8.18e-3}{\centi\metre^2/s}      \\
          $\beta$             &     0.57                                \\
          $S_T$               &     \SI{-4.7e-2}{\kelvin^{-1}}          \\
        \bottomrule
      \end{tabular}
      \label{tab:ludox}
    \end{table}

  \section{Measuring procedure}
  \label{sec:meas}
    The entire measuring procedure has been automated using a custom made Python code to control the temperature and the image acquisition. 
    
    \subsection{Temperature control}
    \label{subsec:temp}
      The control of the temperature of a target system is a well known problem in the realm of Control Theory. The basic scenario consist in a device that
      can impose a heat flux, a sensor that can read the system temperature and a controller that takes the feedback from the sensor and controls the device. 
      Mathematically, this situation is modeled by two quatities: an error function, $e(t)$, and a control function $c(e(t),t)$. The latter is a function
      of the former, as the response of the controller is dependent on the error, that is simply the difference between the target temperature and the measured
      one.

      Among the various protocols employed for this and similar tasks, one of the most used is the \emph{PID control loop}, where the acronym stands for
      \emph{Proportional-Integral-Derivative}. Indeed, the PID control function is made by three terms:
      \begin{description}
        \item[Proportional] it's the most obvious one, a term proportional to the error. In this way, the greater the error, $e(t)$, the greater the response;
        \item[Integral] it's a term proportional to the time integral of the error function, $e(t)$, from the beginning of the control session (or, if the memory
                        of the controller is limited, from a certain amount of time before the measuring time). This term is needed because the only P term doesn't 
                        assure the system to reach the target point. Indeed, there are always heat losses, so that the system may reach a stationary point that
                        is not the target one. The integral term gains sthrength as the time passes, so that such an offset gets eventually corrected;
        \item[Derivative] this last term isn't always needed, but helps in smoothing the response of the control system to peaks in the error function. It's proportional
                          to the derivative of the error function.
      \end{description}
      Therefore, the control function is:
      \begin{equation}
      \label{eq:pid}
        c(e(t),t) = K_\textup{p} e(t) + K_\textup{i} \int_0^t  e(t') dt' + K_\textup{d} \frac{de(t)}{dt}
      \end{equation}
      where $K_\textup{p}$, $K_\textup{i}$ and $K_\textup{d}$ are the coefficients (all non-negative) of the proportional, integral and derivative terms, respectively.
      \Cref{eq:pid} is usually written in a more standard form:
      \begin{equation}
      \label{eq:pid1}
        c(e(t),t) = P \left[ e(t) + \frac{1}{I} \int_0^t  e(t') dt' + D  \frac{de(t)}{dt} \right]
      \end{equation}
      with $P = K_\textup{p}$. In this form, $I$ and $D$ represent, respectively, the integration and derivation times. In this work, the temperature control has been
      made with two Wavelength LFI-3751 Temperature Controller, one for each Peltier element, remotely controlled by a Python code through serial communication in a cascade
      configuration. The $P$,$I$ and $D$ parameters have been accurately tuned in order to have a smooth temperature curve, with minimum overshoot and high disturbance 
      rejection (see~\cref{fig:control} for a representation of the setup and~\cref{fig:T} for a plot of the time evolutions of the temperatures at the borders of the cell). In particular, in our system, the best values are $P = 90.0 $, $I = 1.0$ and $D = 80.0$~\footnote{These values are expressed in the particular scale 
      used by the LFI-3751. Refer  to the user manual of the controller for additional details.}. The temperature sensors employed are two NTC (Negative Temperature 
      Coefficient) thermistors with a nominal resistance of \SI{100}{\kilo\ohm} at \SI{25}{\celsius}.  
      \begin{figure}
      \centering
        \includegraphics[width = \linewidth]{immagini/T_control}
      \caption{The schematics of the temperature control setup. $P_1$ and $P_2$, the two Peltier elements, are controlled by two temperature controllers, which
               use a PID control loop to maintain the target temperatures, $T_1$ and $T_2$. Each one of the controllers receives a feedback from a thermistor (not shown) 
               placed on the external surface of the correspondent sapphire plate. The controllers are remotely driven by a Python program, which automatically sets
               the target temperatures and the $P$, $I$ and $D$ parameters. The Peltier elements are in thermal contact with a heat bath at constant temperature
               $T_0$. In this way, the heating element takes heat from the bath, and the excess heat coming from the cooling element gets removed from the cell.}
      \label{fig:control}
      \end{figure}

    \subsection{Image acquisition}
    \label{subsec:acq}
      The CCD camera is controlled remotely by a computer program, and the interface is a National Instrument PCI-1407 frame-grabber. In order to have an integrated
      environment to program the measurent, Python has been choosen as language for all the operations, for its flexibility and openness. In this way, we have written
      a code that controls every phase of the measurement in a single program. The measurement parameters are given by the user through a configuration file, and 
      include:
      \begin{itemize}
        \item the measuring time;
        \item the path where to save the images;
        \item the $P$, $I$ and $D$ parameters for the two controllers;
        \item the timestep for the acquisition of the images;
        \item the timesteps and temperature steps for the initial temperature ramp.
      \end{itemize}
      The JAI camera used in this work allows to acquire up to, approximately, $10 \div 12$ images per second, working in the grab mode. In this mode, the camera
      constantly fills a buffer of images at the maximum frequency available and returns one of them when prompted. The images captured are 8-bit monochrome pictures
      of $576 \times 768$ pixels, that occupy circa \SI{0.5}{\mega \byte} of memory.  
      
      \begin{figure}
      \centering
      \begin{tikzpicture}
        \begin{axis}[ xlabel = $t(s)$,
                      ylabel = Temperature (\si{\celsius}),
                      clip marker paths=true,
                      legend entries = {$T_1$,$T_2$}]
          \addplot[color = orange,  thin, only marks, mark = *, mark size = 1pt ] table [x = t, y = tb] {grafici/T.dat};
          \addplot[color = teal, thin, only marks,mark = *, mark size = 1pt] table [x = t, y = ta] {grafici/T.dat};
        \end{axis}
      \end{tikzpicture}
      \caption{Experimental data of the time evolution of the temperatures measured on the external surfaces of the sapphire plates. We see that the overshoot is minimal
               and the temperatures are stable. Notice that the two initial temperatures are kept at two slighltly different values to avoid convection.}
      \label{fig:T}
      \end{figure}
