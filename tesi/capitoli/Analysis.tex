\chapter{Statistical analysis}
\label{chap:an}
   
  The quantitative shadowgraph technique, explained in its theoretical and experimental aspects in~\cref{chap:shadow} and~\cref{chap:setup},  relies on the statistical 
  processing of long sequences of images, to recover their static power spectrum, together with their time correlation function. 
  This results in datasets composed of thousands of images of the kind of~\cref{fig:samplepic}, i.e. 8-bit grayscale images. Roughly
  speaking, the bright zones correspond to constructive interference between scattered waves and the dark ones to destructive interference.
  The perturbation of the wavefront introduced by the sample is not the only contribution to the signal recorded by the sensor. 
  Different noise sources contribute to the image saved by the camera. In particular, there are:
  \begin{figure}[tp]
  \centering
    \includegraphics[width = 0.77\linewidth]{immagini/sample}
    \caption{A shadowgraph image. This is the full $576\times768$ picture recorded by the CCD sensor, the analysed image is a $256\times256$ crop centered in the middle
    of the image. Here we can see some of the noise sources explained below. The circular shapes are diffraction patterns caused by dust or imperfections on the surface of 
    the sapphire plates or the other optical elements of the setup. There are also, near the borders, the annular fringes of the diffraction on the border of the 
    cell. Furthermore, the beam profile is clearly visible: there is a peak of the intensity in the centre of the image, while it slightly decreases towards the borders.}
    \label{fig:samplepic}
  \end{figure}
  \begin{description}
    \item[Optical noise (or optical background)] that comes from the dust or eventual halos on the optical elements. This noise is constant in time and the easier to get rid of;
    \item[Environmental noise] that consist in changes in the average intensity of the images, and comes from luminosity variations in the environment. This can be 
          reduced shielding the optical axis during the measurement. 
    \item[Intensity variations] the average intensity of the light beam shows fluctuations in time. 
          %Furthermore, the beam intensity profile, shaped by the achromatic doublet into a plane, is never a perfect plane, but it is usually slightly peaked in the center. 
          During the analysis, the difference in average intensity 
         between different images can be removed normalizing the single picture with their mean intensity. 
    \item[Electronic noise (or electronic background)] that comes from the noise of the electronic parts of the detecting system, in particular from the digital sensor. This is the most 
         difficult one to get rid of, and can greatly affect the goodness of the data. 
    \item[Thermal noise] this noise is unavoidable, and consists in the fluctuations of the refractive index due to the equilibrium thermal agitation. If the 
         non-equilibrium signal is not sufficiently greater than the equilibrium one, the latter can be the prevalent source of the signal.
  \end{description}
  
  As mentioned in~\cref{sub:struct}, from a scattering experiment one can extract the (static and/or dynamic) structure factor of the sample and, as 
  explained in~\cref{sec:near}, in a near-field experiment this is achieved by means of a Fourier analysis carried out on the acquired data. In this chapter
  we will present the analysis procedures to extract the static power spectrum, $S(q)$ of the fluctuations (\cref{sec:sq}) and their correlation time, $\tau(q)$
  (\cref{sec:tau}). These procedures are called, respectively, \emph{static analysis} and \emph{dynamic analysis}. Another quantity that is useful in studying the 
  time evolution of the sample is the variance of the images with respect to a reference image (usually obtained as an average of several images) taken in equilibrium conditions.

  
  \section{Static analysis}
  \label{sec:sq}
    The first step to extract data from the images,in both static and dynamic analysis,  is to crop the relevant region and normalize them with their average value. 
    In our case, the relevant region is a centered square of $256\times256$ pixels. The normalization removes the the differences in the average light intensity 
    due to the environmental noise (stray light) between different images.s
    
    To obtain the power spectrum of the intensity fluctuations in the shadowgraph images it is sufficient to Fourier transform them and take the square modulus. If $I(\m{x},t)$ is the 
    light intensity recorded by the sensor, then the normalized intensity is $i(\m{x},t) = \frac{I(\m{x},t)}{\left<I(\m{x},t)\right>_{\m{x}}}$. Note that the time
    dependence in these quantities is discrete, since the acquisition time, $t$, assumes only discrete values. In conditions of
    weak scattering, the intensity is, approximatively, the sum of two contributions (\cref{eq:scat-int}): the optical background, $I_0 = \left| U_0 \right|^2$ and the signal,
    $2\operatorname{Re}\left[ U_\textup{S}(\m{x},t) U_0^* \right]$. To get rid of the optical background, one takes the average, in the real space, of the normalized
    intensities of the images taken at different times and subtracts it to the normalized intensity of the single images, to obtain:~\footnote{This operation removes also
    another effect of the non ideality of the setup, that is the presence of a non-perfectly flat beam profile. This effect can be seen in~\cref{fig:samplepic}}
    \begin{equation}
    \label{eq:i}
      \delta i (\m{x},t) = i(\m{x},t) - \left< i(\m{x},t) \right>_t = \frac{2\operatorname{Re}\left[ U_\textup{S}(\m{x},t) U_0^* \right]}{\left<I(\m{x},t)\right>_{\m{x}}}
    \end{equation}
    where we have also assumed that the scattered signal has zero average in time. $\delta i (\m{x},t)$ represents the fluctuations of the intensity of the shadowgraph
    images at time $t$, due to the non-equilibrium fluctuations of the refractive index and to the other noise sources summarized above. 
    To separate the contributions of the different Fourier components to the signal, we must take the Fourier transform of $\delta i (\m{x},t)$. Taking,
    then, the square modulus of the transform, we obtain the power spectrum:
    \begin{equation}
    \label{eq:pow-spec}
      S_\textup{m}(\m{q}, t) = \left| \delta i (\m{q}, t)  \right|^2  
    \end{equation}
    where the subscript stands for \emph{measured}. Then, to obtain the final value of the measured static structure factor of the sample, one takes the average 
    of~\cref{eq:pow-spec} over all the images:
    \begin{equation}
    \label{eq:final-sq}
      S_\textup{m} (\m{q}) = \left< S_\textup{m}(\m{q}, t) \right>_t = \left< \left| \delta i (\m{q}, t)  \right|^2 \right>_t
    \end{equation}
    Indeed, in this value there is still the contribution of the electronic noise:
    \begin{equation}
    \label{eq:meas-spec1}
       S_\textup{m} (\m{q}) = T(\m{q})S(\m{q}) + B(\m{q})
    \end{equation}
    that is the same of~\cref{eq:meas-sp}.
    Due to the circular geometry of the cell employed in this work (see~\cref{sec:app}), the images, as well as all the quantitities obtained starting from them, 
    display an azimuthal simmetry. So the dependence on the wavevector, $\m{q}$, it's actually a dependence on the modulus $q = \left| q\right|$. This fact also increases the
    statistics for the high-\emph{q} values. As a consequence, we can perform an azimuthal average, to obtain:
    \begin{equation}
    \label{eq:meas-spec2}
      S_\textup{m} (q) = T(q)S(q) + B(q)
    \end{equation}
   
    \subsection{Implementation}
    \label{subsec:stat-im}
      The static procedure described has been implemented in a Python code. In the Python universe there are a lot of packages that allow to carry out 
      data analysis tasks with efficiency and speed. In particular, the Scipy ecosystem, of which the core element is the Numpy package, with its multimensional
      array data structure, the \emph{ndarray}, comprises many tools useful for efficient scientific computing~\cite{Virtanen2020,5725236}. 
      We have written a  program that allows the user to choose the type of analysis (\emph{static}, \emph{dynamic}, \emph{variance}) to carry out, simply specifying the path to 
      the directory containing the images and the parameters of the analysis. In the following the details of the static algorithm will be discussed. The dynamic algorithm
      will be described in~\cref{sec:tau}.
   
      In this work, the basic data structure is an image, i.e. a $256\times256$ matrix of integer values in the range $[0,255]$. Once imported in memory, each image
      is saved in a Numpy array. Since every subsequent operation has to be made on the normalized images, the normalization is made during the importing operation, and
      the array gets casted to a double precision data type. The static algorithm implements the operations described in the previous paragraph and  comprises the following steps:
      \begin{enumerate}
        \item computation of the optical background $\left< i(\m{x},t) \right>_t$. It's simply the arithmetic average of all the images;
        \item computation of the difference $\delta i (\m{x},t)$;
        \item Fourier transform of $\delta i (\m{x},t)$;
        \item square modulus of $\delta i (\m{q},t)$;
        \item computation of the azimuthal average $S_\textup{m} (\m{q})$
        \item at this point the power spectrum of the single image is stored in a Pandas DataFrame. This data structure allows to perform 
              statistical operations on large datasets in a quick and efficient way~\cite{McKinney2010}. Each row of the DataFrame is an array containing
              the values of $S_\textup{m}(q)$ computed at a certain $t$, i.e. for a single image. With $256\times256$ images, the number of different $q\text{-values}$, 
              obtained considering circular annuli 1-pixel wide is $182$, so that, in the end, all the static information coming from the experiment is contained in
              a DataFrame of $10000$ rows and $182$ columns. This accounts for, approximatively, \SI{15}{\mega\byte} of memory, that is a very small 
              amount.\footnote{Indeed, memory management is not a problem in the static analysis, since all the operations are meade on a single image at a time.}; 
        \item in the end, the mean of all the power spectra contained in the DataFrame is taken.
      \end{enumerate}
      A flowchart representation of the algorithm is given in~\cref{fig:flow-stat}. To compute the value of $B(q)$ it is necessary to perform a separate measurement 
      with the same sample, taking the same number of images (to achieve the same precision), in thermal equilibrium conditions. Indeed, in this way, the power spectrum
      of the non-equilibrium signal, $S(q)$, is zero and, from~\cref{eq:meas-spec2}, $S_\textup{m} (q) = B(q)$.~\footnote{Actually, in this conditions there is a small
      equilibrium contribution $T(q)S_\textup{eq}(q)$ that, in this way gets characterized together with $B(q)$ and subtracted to the measured power spectrum.}
      \begin{figure}[]
      \centering
        \begin{tikzpicture}[node distance=2cm]
          
          \node (opti) [block,align = center] {Optical background\\ $\left< i(\m{x},t) \right>_t$};
          \node (diff) [block, below of = opti,align = center] {Image difference\\ $\delta i (\m{x},t)$};
          \node (fou) [block, below of = diff,align = center] {Fourier transform\\ $\delta i (\m{q},t)$};
          \node (square) [block, below of = fou,align = center] {Square modulus\\ $\left| \delta i (\m{q},t) \right| ^ 2$};
          \node (azim) [block, below of = square,align = center] {Azimuthal average};
          \node (powe) [block, below of  = azim,align = center] {Power spectrum\\ $S_\textup{m}(q) = \left< \left< \left| \delta i (\m{q},t) \right| ^ 2 \right>_t \right>_{\hat{\m{q}}}$};
    
          \draw [arrow] (opti) -- (diff);
          \draw [arrow] (diff) -- (fou);
          \draw [arrow] (fou) -- (square);
          \draw [arrow] (square) -- (azim);
          \draw [arrow] (azim) -- (powe);
        \end{tikzpicture}
      \caption{Flowchart of the static algorithm. The steps to obtain $B(q)$ are the same, with a measurement in equilibrium conditions.}
      \label{fig:flow-stat}
      \end{figure}
      
      Regarding the performance of the algorithm, the bottlenecks of the whole procedure are the I/O operations, in particular loading the images from the disk. We found that 
      the most efficient way to read a image is using the \texttt{imread()} function of the package OpenCV~\cite{opencv_library}. We tested this method against the 
      equivalent ones of other libraries (Scipy, Scikit-image, Pillow) and we found that, in terms of speed, it is the best one. The other computationally heavy operations 
      are the Fourier transform and the azimuthal average. Regarding the transform, we used the Numpy FFT implementation, that makes use of the Intel-MKL optimizations 
      under the hood to perform the operation faster on Intel processors~\cite{intelMKL}. For the azimuthal average, we wrote a custom function, 
      \texttt{azimuthalAverageFast()}, based on a contribution by Bi Rico on the Q/A site Stack Overflow~\cite{azimuth}. Since the algorithm operates in a 
      sequential way, the time needed to perform the analysis grows linearly with the number, $N$, of images to analyse. With a dataset of $10000$ samples 
      (already cropped to $256\times256$), the time needed on a computer with a Intel Core-i5 \SI{3.5}{\giga\hertz} processor and \SI{8}{\giga\byte} RAM is of the order 
      of \SI{1/2}{\hour}.

  \section{Dynamic analysis}
  \label{sec:tau}
    As mentioned in~\cref{sub:struct}, another valuable information about the NEFs comes from the time correlation properties. These are related to the dynamics of the 
    phenomenon, 
    that is how the fluctuations evolve with time. In particular, the quantity of interest is the correlation time, $\tau = \tau(q)$, of the fluctuations. This variable,
    that, for non-equilibrium phenomena is often called the \emph{relaxation time}, is the time needed for the system to ``relax'' from non-equilibrium to 
    equilibrium, or, more specifically, for the fluctuations with wavevector $q$ to die out. Before giving a more formal definition of $\tau(q)$, we need to introduce 
    the temporal autocorrelation function, $G(q,dt)$, with $dt$ the delay at which evaluate the correlation, where we have already dropped the dipendence on 
    the direction of $\m{q}$ because of the isotropy of our system. The temporal autocorrelation function of non-equilibrium concentration fluctuations is defined as:
    \begin{equation}
    \label{eq:temp-autocor}
      G(q,dt) = \left< \delta c (t) \delta c^* (t+dt)  \right>_t
    \end{equation}
    with $\delta c (t)$ being the fluctuating part of the concentration and the brackets representing the time average. $G(q,dt)$ is the Fourier transform of the structure factor of the fluctuations. Since, as we have 
    seen in~\cref{eq:lorentz}, this is approximatively a lorentzian curve, it is reasonable to assume that, for NEFs, $G(q,t,dt)$ has an exponential form:
    \begin{equation}
    \label{eq:time-corr}
      G(q,dt) = G(q,0) \exp \left( -\frac{dt}{\tau(q)} \right)
    \end{equation}
    In this way the definition of $\tau(q)$ becomes clear: it's the characteristic decay time of the fluctuations. The temporal autocorrelation function is usually measured 
    in dynamic scattering experiment. In cases when the system is isotropic, it
    reduces to the time dependent radial distribution function. Like in static scattering, also in dynamic scattering, when performed in far-field conditions, the 
    Fourier transform of the scattered signal is performed by propagation (or by means of optical elements, like lenses) and the measured intensity allows to retrieve with 
    some processing, the dynamic structure factor. In near-field experiments, the same information can be retrieved by means of a differential Fourier analysis, firstly 
    developed by Croccolo et al.~\cite{Croccolo2006}, and later extended also to
    microscopy experiment (with white, non-coherent light), where it takes the name of \emph{Differential Dynamic Microscopy (DDM)}~\cite{Cerbino2008}. The central idea of this
    technique, the dynamic analysis, is that, instead of calculating the spatio-temporal Fourier transform of the signal to obtain the temporal correlation function, one 
    performs a spatial Fourier analysis of image \emph{differences}. This has a twofold advantage: on the one hand, it simplifies the procedure, because, as we will see, only 
    spatial Fourier transformation is needed; on the other hand, it acts like a high-pass filter, effectively removing the optical background from the signal. This last 
    statement is true only if the analysis is made taking images at a relative temporal distance bigger than the biggest characteristic time of the sample. Otherwise, part of 
    the signal gets subtracted together with the optical background, that is the same for every image. 
    The procedure of calculating the static structure factor of image differences leads to a function, called \emph{structure function}, that, as we will see in
    the following, is related to \emph{both} the static structure factor and the temporal autocorrelation function. 
    
    We start by considering the intensity difference $\delta i (\m{x}, t, dt) = i (\m{x},t + dt) - i(\m{x}, t)$ of two normalized images. The measured structure function, 
    $C_\textup{m} (\m{q}, t, dt)$, is the square modulus of the spatial Fourier transform of $\delta i (\m{x}, t, dt)$, averaged over all the pairs of images with the 
    same delay $dt$:
    \begin{equation}
    \label{eq:struc-fun}
      C_\textup{m} (\m{q}, t, dt) = \left<  \left| \delta i (\m{q}, t, dt) \right|^2\right>_t
    \end{equation}
    The right hand side of this equation can be expanded as follows:
    \begin{equation}
    \label{eq:ex}
      \begin{split}
        \left<  \left| \delta i (\m{q}, t, dt) \right|^2\right>_t &= \left<  \delta i (\m{q}, t, dt) \delta i^* (\m{q}, t, dt) \right>_t\\ 
        &= \left< \left[ i (\m{q},t + dt) - i(\m{q}, t)  \right] \left[ i (\m{q},t + dt) - i(\m{q}, t)  \right]^*  \right>_t
      \end{split}
    \end{equation}
    so the structure function is equivalent to:
    \begin{equation}
    \label{eq:str1}
      C_\textup{m} (\m{q}, t, dt) = 2 \left< \left|  i (\m{q}, t, dt) \right|^2  \right>_t - 2 \operatorname{Re} \left< i(\m{q}, t) i^* (\m{q},t + dt)  \right>_t
    \end{equation}
    The first term is related to the measured static structure factor. Indeed, from~\cref{eq:final-sq} and~\cref{eq:meas-spec1}, we see that:
    \begin{equation}
    \label{eq:str2}
      \left<  \left|  i(\m{q},t) - i_0(\m{q})  \right|^2 \right>_t = \left< \left| i(\m{q},t) \right|^2  \right>_t - \left|i_0(\m{q}) \right|^2 
      = T(\m{q})S(\m{q}) + B(\m{q})   
    \end{equation}
    where $i_0(\m{q})$ is the Fourier transform of the optical background $\left< i(\m{x},t) \right>_t$. The second term of~\cref{eq:str1} is related to the 
    temporal autocorrelation function $G(\m{q},t,dt)$ by the relation:
    \begin{equation}
    \label{eq:str3}
      \begin{split}
        \operatorname{Re} \left< \left[ i(\m{q}, t) - i_0(\m{q}) \right] \left[ i^* (\m{q},t + dt) - i^*_0(\m{q}) \right]  \right>_t &=\\
        \operatorname{Re} \left< i(\m{q}, t) i^* (\m{q},t + dt)  \right>_t - \left| i_0(\m{q}) \right|^2 &= T(\m{q})S(\m{q})G(\m{q},t,dt) 
      \end{split}
    \end{equation}
    where we have neglected a contribution given by the temporal autocorrelation of the noise, because we assume that it is a delta function centered in 
    zero delay time. Finally, combining~\cref{eq:str1} with~\cref{eq:str2,eq:str3}, we obtain:
    \begin{equation}
    \label{eq:cm}
      C_\textup{m} (\m{q}, t, dt) = 2 \left \{ T(\m{q})S(\m{q}) \left[1 - G(\m{q},dt)  \right] + B(\m{q})  \right \}
    \end{equation}
    As we can see from the last equation, the structure function contains both the static structure factor and the temporal autocorrelation function. As a 
    consequence, the dynamic analysis is a powerful tool not only because it gives acces to the the dynamics information of the sample, but also because, 
    at the same time, gives the same information of the static analysis. Once again, because of the azimuthal simmetry of our system, we can perform an
    azimuthal average on the wavevectors and the dependence of the structure function is only on their magnitude. From~\cref{eq:cm}, it is possible
    to obtain $T(q)S(q)$ and $B(q)$ as the free parameters of a fit of $ C_\textup{m} (q, t, dt)$ as a function of the delay $dt$, for each value of $q$.
    In this way, we can invert the structure function, and obtain the temporal autocorrelation function:
    \begin{equation}
    \label{eq:gt}
      G(q,dt) = 1 + \frac{2B(q) - C_\textup{m} (q, t, dt)}{2 T(q)S(q)}
    \end{equation}
    \begin{figure}[htp]
      \centering
          \begin{tikzpicture}%
            \begin{axis} [   xlabel = $dt$,
	                           ylabel = $C_\textup{m} (dt)$,
                             xmin = 0.1,
                             xmax = 100,
                             width = 0.7\linewidth,
                             legend entries = {$C_\textup{m} (dt)$,$2B(q)$,$2T(q)S(q)$ },
                             legend style = {at={(axis cs:91,10.1)},anchor=south east},
                             yticklabels={,,},
                             xticklabels={,,}]%
              \addplot[domain = 0.1:100, samples = 100, smooth, thick, teal] {1-exp(-x/10)+ 10};
              \addplot[domain = 0.1:100, samples = 100, smooth, thick, Coral3, dashed] {10};
              \addplot[domain = 0.1:100, samples = 100, smooth, thick, DodgerBlue3, dashed] {11};
	          \end{axis}%
          \end{tikzpicture}%
        \caption{Plot of the structure function.}
        \label{plot:struct-fun}
      \end{figure}
    
    \subsection{Implementation}
    \label{subsec:dyn-im}
      In the same program described in~\cref{subsec:stat-im} the user can choose (when prompted, at the launch) to perform the dynamic analysis on the data. 
      The images are acquired by the camera at a nearly constant rate, but there is a minimal variability in the framerate. This results in a dataset where 
      the time distance between consecutive images isn't constant, but follows a certain distribution, centered around the selected timestep. As a consequence,
      the timestep is not a suitable quantity to use as a timestep for the algorithm. For this reason, a different strategy has been used. The main loop of the 
      algorithm runs iterating on the images, rather than on the times, using as unit for the iteration the \texttt{image\_step}, an integer positive value. The algorithm
      selects a subset of the dataset of images whose index is spaced by \texttt{image\_step}, to which apply the procedure. This is particularly useful, because
      allows the user to vary the time resolution of the analysis, based on the charactheristic times of the phenomenon of interest, without changing the code.
      Once the user has inserted the value of \texttt{image\_step}, he/she gets prompted to insert also the \texttt{time\_inf} and \texttt{time\_sup}, to select
      the interesting part of the measure, and \texttt{max\_dt}, to select the maximum delay value to consider in the analysis. Furthermore, the user has to insert
      a value for the binning of the delay times. By default, the program divides the delay times axis into bins spaced by the acquisition timestep (the most frequent
      value), but a higher value can be (and it's usually the case) choosen. At this point the program performs the dynamic analysis described in the previous paragraph.

      We have developed a new way to implement the dynamic procedure that will be described in the following. This procedure exploits the fact that, nowadays, memory
      availability and cost are not anymore a problem as it used to be in the past, so that it is common to have, for example \SI{8}{\giga\byte} of RAM on a 
      mid-range personal computer. This allows to easily load in the memory up to $10000$ double precision $256\times256$ images. Another important aspect
      that we leveraged in our approach is the linearity of most of the operations applied to the images in the dynamic procedure, where the only nonlinear operation
      is the square modulus. This enables one to change the order of the previous operations. The steps of the algorithm are:
      \begin{enumerate}
        \item load the selected images in a buffer;
        \item perform the FFT of the images contained in the buffer and save them back to the buffer, in place. Since this operation operates on the single images 
              separately, it has been possible to parallelize it on the processor cores to gain speed. Performing the FFT on the single image, once, rather than
              compute the FFT of the differences greatly reduces the number of times that the FFT has to be performed. Indeed, if $N$ is the number of images
              to analyse, supposing that we consider every possible difference, the number of differences goes like the number of unique pairs of images,
              that is $N(N-1)/2$, a quantity that is greater than $N$ for $N=3$, and scales like $O(N^2)$ for high numbers of images.
        \item for each pair of images (using two nested \texttt{for} loops):
              \begin{enumerate}
                \item calculate the time difference between the two images considered;
                \item compute the  difference between the FFTs of the images;
                \item calculate the square modulus of the difference; 
                \item take the azimuthal average of the result (inside the inner of the two nested loops);
                \item add the result in the correspondent row of the \texttt{results} matrix (see below);
              \end{enumerate}
        \item take the arithmetic average of the results, averaging on the number of pairs with the same delay time value.
      \end{enumerate}
      The \texttt{results} matrix is a matrix with \texttt{bin\_number} rows and \texttt{q\_number} columns, where \texttt{bin\_number} is the number of bins 
      in which the delay axis has been divided and \texttt{q\_number} is the number of $q\text{-points}$ coming from the azimuthal average, that, for $256\times256$ images
      takes the value of $182$. To better understand, it follows a representation of the matrix: 
      \[
         \begin{blockarray}{cccc}
              & q_0 & \dots & q_{181} \\
         \begin{block}{c(ccc)}
         dt_0 &  C_\textup{m} (q_0, dt_0) & \dots & C_\textup{m} (q_{181}, dt_0)  \\
           \vdots  &  & \ddots   &   \\
         dt_{bin\_number}  &  &  & C_\textup{m} (q_{181}, dt_{bin\_number})  \\
         \end{block}
         \end{blockarray}
          \]
      In this way, at the end of the procedure, we have all the structure functions for all the wavevectors and delay values, contained in a single data structure and 
      obtained in a single run of the algorithm. 

      \begin{figure}[]
      \centering
        \begin{tikzpicture}[node distance=2cm]
          
          \node (buff) [block,align = center] {Load the images in a buffer};
          \node (fft) [block, below of = buff,align = center] {Perform the FFT of the images\\ $i (\m{q},t)$};
          \node (diff) [block, below of = fft,align = center] {Compute the difference\\ $\delta i (\m{q},t,dt) = i (\m{q},t+dt) - i (\m{q},t)$  };
          \node (square) [block, below of = diff,align = center] {Square modulus\\ $\left| \delta i (\m{q},t) \right| ^ 2$};
          \node (azim) [block, below of = square,align = center] {Azimuthal average};
          \node (t) [block, right of = azim,align = center, xshift = 3cm] {repeat for all possible $dt$};
          \node (cm) [block, below of  = azim,align = center] {Structure function \\ $C_\textup{m} (q, dt) = \left< \left< \left| \delta i (\m{q},t,dt) \right| ^ 2 \right>_t \right>_{\hat{\m{q}}}$};
          \node (save) [block, below of = cm,align = center] {Save in the \texttt{results} matrix};
          
    
          \draw [arrow] (buff) -- (fft);
          \draw [arrow] (fft) -- (diff);
          \draw [arrow] (diff) -- (square);
          \draw [arrow] (square) -- (azim);
          \draw [arrow] (save) -| (t);
          \draw [arrow] (t) |- (diff);
          \draw [arrow] (azim) -- (cm);
          \draw [arrow] (cm) -- (save);
        \end{tikzpicture}
      \caption{Flowchart of the dynamic  algorithm.}
      \label{fig:flow-dyn}
      \end{figure}
       
      Regarding the performance of the algorithm, the same optimizations of the static procedure have been applied. Here, the main factor that impacts on the execution
      time is the great number of differences between images. There isn't much that can be done to optimize this operation, apart from parallelizing the code. This has been
      done on the processors cores, but it  doesn't add a significant gain. Probably more speed can be attained parallelizing on a GPU. Anyway, even without parallelizing,
      the program is quite fast. On the same machine used for the static analysis, it runs in about an hour.
      
       
      
  \section{Statistical averaging}
  \label{sec:stat}
    In both the static and dynamic analysis procedures described in this chapter, averages of images and of quantities obtained from them are computed. However, the
    statistical significance of such averages has to be taken into account. Indeed, averaging on the time series of images allows to obtain ensemble averages only
    if (and when) the ergodic hypothesis is valid. In studying time-dependent process like the transient process that brings the system from equilibrium to a steady
    non-equilibrium state, system can be considered ergodic only on short time scales. 

    There is another aspect to consider when choosing the timestep in the acquisition stage and in the processing one, that is the correlation time of the process of interest.
    Indeed, if one averages images that are taken at a time interval shorter than the correlation time, these images are not statistically independent, so that they don't
    contribute as  different samples to the average. For the same reason, when doing the dynamic analysis, it is important to only take differences between images
    with a relative time distance greater than the correlation time, otherwise the signal gets removed together with the optical background.
