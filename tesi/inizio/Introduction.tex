\chapter{Introduction}
\label{chap:intro}

In every fluid system at a finite temperature the values of the thermodynamic variables that define the state of the system fluctuate locally 
around their equilibrium values, due to thermal agitation. 
These \emph{equilibrium fluctuations} are short ranged, apart from when the system is near a critical point, and usually have small amplitude. 
\emph{Non-equilibrium fluctuations (NEFs)} develop in fluids and fluid mixtures whenever there is a macroscopic gradient in one or more of the thermodynamic variables 
(for example, in the presence of a concentration gradient), and are drastically different from equilibrium ones. They are always 
long-ranged, not only near a critical point, and their amplitude is greatly enhanced with respect to the amplitude 
of equilibrium fluctuations.% Indeed, their power spectrum, as a function of the wavevector magnitude, $q$, shows a $\sim q^{-4}$ power law scaling.

From a theoretical point of view, NEFs represent the missing link in our understanding of the mass transport phenomena in fluids. 
Indeed, while \emph{diffusion}, the microscopic motion of a substance driven by concentration inhomogeneities and \emph{convection},
the macroscopic motion of portions of fluid driven by gravity, are well understood as two distinct phenomena, NEFs, which take place at a 
mesoscopic level, are the phenomenon at the very basis of both diffusion and convection. In fact, it has been shown that non-equilibrium fluctuations
cause a net mass transfer that coincides with the usual Fickean diffusion and that convection is started by them~\cite{Brogioli2001}. 
Furthermore,  recent works have predicted that, in fluid mixtures, the confinement of non-equilibrium  fluctuations should give rise to long-range interactions between the particles 
that would cause very strong Casimir-like forces in the fluid~\cite{Kirkpatrick2013,Kirkpatrick2015}.

The interest in NEFs is also given by the potential practical applications. Indeed, because they are directly related to the mass transport properties of the 
fluids, or mixtures, where they take place, measuring their dynamics properties gives direct access to the transport coefficients of the system, without requiring
any further measurements. This could lead to the development of highly efficient diagnostic tools for the characterization of complex fluids. 

In the past decades, a great effort has been made in the study and characterization of non-equilibrium fluctuations in fluids and fluid mixtures. The statics and 
dynamics properties of temperature and concentration NEFs have been experimentally investigated, mainly in stationary non-equilibrium conditions or in free 
diffusion.  
Moreover, since NEFs are greatly affected by gravity, which quenches
the amplitude of the fluctuations above a critical size, space experiments have been carried out to study them in microgravity conditions, with results
that confirm the theoretical predictions~\cite{Vailati2011}.

Presently, a number of questions about NEFs remain open. In particular, it would be interesting to investigate extensively: the non-equilibrium fluctuations in complex 
mixtures made by more than two components, in relation to their transport coefficients; the non-equilibrium fluctuations in polymer mixtures, in relation to both their transport
coefficients and
their behaviour close to a glass transition; the effect of microgravity conditions on NEFS; the development of NEFs and the transient processes that lead to the non-equilibrium 
steady state; the non-equilibrium fluctuations in concentrated colloidal suspensions, a problem that is related to the detection of non-equilibrium 
Casimir forces, that still lack an experimental evidence.
To answer these and other questions, an international 
effort is being done and a new series of space experiments, in the framework of the \emph{Giant Fluctuations} project of the European Space Agency, will be launched starting
from 2022
to carry out experiments on NEFs aboard the International Space Station~\cite{Baaske2016a}. 


In this thesis work, which is part of the \emph{Giant Fluctuations} project, we have dealt with the investigation of a thermophilic colloidal suspension under the action 
of a temperature gradient. We have characterized the convective instabilities that may arise near the 
convective threshold and the 
concentration NEFs  that will be studied in space by the project, with the goal of investigating the transient regime and the 
kinetics of growth of the NEFs during a thermodiffusion process. The non-equilibrium condition is attained exploiting the \emph{Soret effect}, or thermodiffusion, which 
allows to obtain a concentration gradient imposing a temperature gradient to the sample. To study this system we employed \emph{shadowgraph}, a 
near-field light scattering technique that allows to retrieve the correlation properties of the sample analyzing the interference pattern of a reference beam with the light scattered by
the refractive index fluctuations in the sample, which are directly related to the concentration fluctuations. The shadowgraph images have been analysed with 
a custom made implementation of an analysis procedure that allows to retrieve their statistical properties, called \emph{dynamic analysis}~\cite{Croccolo2006}. We have implemented it using
the Python programming language, focusing on the efficiency of the code.


In the first part of the work, we explored the conditions under which convective instabilities develop inside the sample, in order to find the best conditions to 
study the fluctuations. We found that the conditions under which the sample is prepared affect its stability, even when the applied temperature gradient
is kept under the convective threshold. In particular, we observed and qualitatively characterized the onset of localized convection patterns, which 
exhibit rotating dynamics not yet observed under the conditions used in our study, where the accumulation of the particles at the bottom of the cell determined 
by the Soret effect should prevent the onset of convective instabilities.

Subsequently, we proceeded to study the statics and the dynamics of the concentration  non-equilibrium fluctuations, carrying out experiments with different values of 
the temperature gradient and of the concentration of the colloidal particles. What we found is that, even with high temperature gradients, 
the signal coming from the non-equilibrium fluctuations is weak with respect to the equilibrium one and the background noise. This result is  unexpected, as the 
highly thermophilic behaviour of the chosen colloidal particles, LUDOX\reg TMA, suggests that the amplitude of non-equilibrium fluctuations should be large. This outcome opens the way for further studies,
and could be related to the aforementioned Casimir forces which could determine a slowing down of the dynamics and a breaking of  the ergodicity of the sample. This, at the present
moment, remains a speculative hypothesis that will need more investigation.

